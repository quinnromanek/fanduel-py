FROM python:3.7-buster

RUN apt update && apt install -y libpq-dev \
                                 gcc \
                                 python3-dev \
                                 musl-dev \
                                 python3-numpy \
                                 python3-scipy \
                                 glpk-utils

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH "${PYTHONPATH}:/usr/src/app:/usr/lib/python3/dist-packages"

RUN pip install --upgrade pip
RUN pip install pandas==1.0.0


