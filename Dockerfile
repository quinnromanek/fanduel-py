FROM python:3.7-buster

RUN apt update && apt install -y libpq-dev \
                                 gcc \
                                 musl-dev \
                                 glpk-utils

WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH "${PYTHONPATH}:/usr/src/app:/usr/lib/python3/dist-packages"

RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

COPY . /usr/src/app/
COPY ./clean-logs.sh /clean-logs
RUN chmod a+x /clean-logs
ENV QUINNBA_HOST "0.0.0.0"
ENV AIRFLOW_CORE_DAGS_FOLDER "/usr/src/app/quinnba/tasks"
ENV AIRFLOW_CONN_QUINNBA_DB ${DATABASE_URL}
ENV AIRFLOW__API__AUTH_BACKEND "airflow.api.auth.backend.basic_auth"
ENV AIRFLOW_HOME "/usr/src/app/airflow"
ENV FLASK_APP "quinnba.server.app"
CMD [ "gunicorn", "quinnba.server.app:app", "-w", "2", "--threads", "2", "-b", "0.0.0.0:5000" ]




