import logging
import math
import sys
import numpy as np
from sklearn import clone
from sklearn.compose import TransformedTargetRegressor
from sklearn.preprocessing import PowerTransformer
from sklearn.utils import resample

np.seterr(divide="ignore", invalid="ignore")

from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.linear_model import Ridge
from sklearn.model_selection import RepeatedKFold, GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.tree import DecisionTreeRegressor

from quinnba.db import Session, PlayerModel, PlayerPrediction
from quinnba.aggregation import load_training_input
from typing import Optional, List
import patsy
from quinnba.nba import fanduel_points
import pandas as pd


log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
log.addHandler(handler)

FEATURES = [
    "pts_avg",
    "pts_opp",
    "reb_avg",
    "reb_opp",
    "ast_avg",
    "ast_opp",
    "stl_avg",
    "stl_opp",
    "blk_avg",
    "blk_opp",
    "to_avg",
    "to_opp",
    "pf_avg",
    "pf_opp",
    "min_avg",
    "usg_pct_avg",
    "min_team_delta",
    "usg_pct_team_delta",
    "game_1",  # Played a game 1 day ago
    "game_2",  # Played a game 2 days ago
    "game_3",  # Played a game 3 days ago, etc...
    "game_4",
    "game_5",
    # "start",
]

MODELS = {
    "decision_tree": DecisionTreeRegressor(),
    "ridge_regression": Ridge(),
}

MODEL_PARAMS = {
    "decision_tree": {
        "lr__max_depth": [i for i in range(2, 10)],
    },
    "ridge_regression": {"lr__alpha": [0.1 * i for i in range(11)]},
}

MODEL_EQUATION = (
    "fpts ~ pts_avg + pts_avg:pts_opp + pts_opp + "
    "reb_avg + reb_avg:reb_opp + reb_opp + "
    "ast_avg + ast_avg:ast_opp + ast_opp + "
    "stl_avg + stl_avg:stl_opp + stl_opp + "
    "blk_avg + blk_avg:blk_opp + blk_opp + "
    "to_avg + to_avg:to_opp + to_opp + usg_pct_avg + "
    # "start + min_delta + min_delta:min_avg +"
    "game_1 + game_2 + game_3 + game_4 + game_5 + "
    "game_1:min_avg + game_2:min_avg + game_3:min_avg + game_4:min_avg + game_5:min_avg + "
    "min_team_delta + min_team_delta:min_avg + "
    "usg_pct_team_delta + usg_pct_team_delta:min_avg"
)


def most_recent_model(s: Session) -> Optional[PlayerModel]:
    return (
        s.query(PlayerModel)
        .filter_by(active=True)
        .order_by(PlayerModel.created_at.desc())
        .first()
    )


def extract_model_data(df: pd.DataFrame, equation=MODEL_EQUATION):

    y, df = patsy.dmatrices(
        equation,
        df,
        return_type="dataframe",
    )
    y = np.ravel(y.to_numpy())
    X = df
    return X, y, df


def train_new_model(
    seasons: List[int], samples, s: Session, points=fanduel_points, save=True
):

    input = load_training_input(seasons, points, s)

    model, score = select_best_model(input)

    models = [resample_model(model, input) for _ in range(samples)]

    if save:
        s.add(
            PlayerModel(
                seasons_hash=hash(tuple(seasons)),
                features_hash=hash(tuple(FEATURES)),
                model=models,
                neg_mean_squared_error=score,
                equation=MODEL_EQUATION,
                samples=samples,
            )
        )

        s.commit()


def resample_model(estimator, input: pd.DataFrame):
    resampled = resample(input, n_samples=math.floor(np.sqrt(len(input))))
    X, y, _ = extract_model_data(resampled)
    m = clone(estimator)
    m.fit(X, y)
    return m


def select_best_model(df: pd.DataFrame):
    X, y, df = extract_model_data(df)

    fs = SelectKBest(score_func=f_regression)
    pt = PowerTransformer()

    all_results = {}
    for name, model in MODELS.items():
        cv = RepeatedKFold(n_splits=min(10, len(df)), n_repeats=3, random_state=2)
        # pipeline = Pipeline([("pt", pt), ("sel", fs), ("lr", model)])
        # ttr = TransformedTargetRegressor(
        #     transformer=PowerTransformer(), regressor=pipeline
        # )
        pipeline = Pipeline([("sel", fs), ("lr", model)])
        # grid = {
        #     "regressor__sel__k": [i for i in range(1, X.shape[1] + 1)],
        # }
        grid = {
            "sel__k": [i for i in range(1, X.shape[1] + 1)],
        }
        grid.update(MODEL_PARAMS[name])

        search = GridSearchCV(
            pipeline,
            grid,
            scoring="neg_mean_squared_error",
            cv=cv,
            n_jobs=-1,
            refit=True,
        )
        results = search.fit(X, y)

        all_results[name] = results

    for name, results in all_results.items():
        log.info("[%s] Best MSE: %.3f" % (name, results.best_score_))

        log.info("[%s] Best Config: %s" % (name, results.best_params_))

        # sel = results.best_estimator_.regressor_.named_steps["sel"]
        sel = results.best_estimator_.named_steps["sel"]

        vals = list(zip(list(df.columns), sel.scores_))
        vals.sort(reverse=True, key=lambda x: x[1])
        for i, val in enumerate(vals):
            print(f"{i}:\t{val[0]}\t{val[1]}")

    _, result = max(all_results.items(), key=lambda x: x[1].best_score_)
    return result.best_estimator_, result.best_score_


def already_predicted(date, model: PlayerModel, session: Session) -> bool:
    return (
        session.query(PlayerPrediction)
        .filter(PlayerPrediction.mid == model.id)
        .filter(PlayerPrediction.date == date)
        .first()
        is not None
    )


def store_predictions(predictions: pd.DataFrame, eng):
    predictions.set_index(["gid", "date"]).to_sql(
        PlayerPrediction.__tablename__, eng, if_exists="append", index=True
    )


def predict(model: PlayerModel, input: pd.DataFrame) -> pd.DataFrame:
    input["fpts"] = 0
    X, _, _ = extract_model_data(input, model.equation)

    predictions = pd.DataFrame(
        {
            i: pd.Series(m.predict(X), index=input.index)
            for i, m in enumerate(model.model)
        },
    )

    input["stddev"] = predictions.std(axis=1)
    input["var"] = predictions.var(axis=1)
    input["pred"] = predictions.mean(axis=1)
    input["samples"] = len(model.model)
    input["mid"] = model.id

    return input.drop(columns=["fpts"])[
        ["gid", "date", "season", "stddev", "var", "pred", "samples", "mid"]
    ].fillna(0.0)
