from quinnba.server.database import db
import pandas as pd
from flask import Blueprint, jsonify
from quinnba.db import Contest, ContestDay
from typing import Dict
from numpy import nan_to_num

contests = Blueprint("contests", __name__)


def _tier_obj(rank, payout, entries):
    if rank < 1.0:
        rank = int(entries * rank)
    return {
        "rank": rank,
        "payout": payout,
    }


@contests.route("/api/contests")
def list_contests():
    cs = db.session.query(Contest).all()

    contests = [
        {
            "id": c.id,
            "name": c.name,
            "entries": c.entries,
            "cost": c.cost,
            "tiers": [_tier_obj(rank, payout, c.entries) for rank, payout in c.tiers],
            "days": [d.day for d in c.days],
        }
        for c in cs
    ]

    return jsonify(contests=contests)


@contests.route("/api/contests/<contest_id>", methods=["GET", "UPDATE"])
def contest(contest_id):
    pass
