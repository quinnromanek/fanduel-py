from quinnba.server.database import db
from quinnba.db import Player, MissingPlayer, fix_gid

from flask import Blueprint, jsonify, request

player = Blueprint("player", __name__)


@player.route("/api/players/missing_gid", methods=["GET", "POST"])
def handle_missing():
    if request.method == "POST":
        data = request.json
        nbaid = data["nbaid"]
        gid = data["gid"]
        fix_gid(nbaid, gid, db.session)

    return jsonify(
        missing=[
            {
                "name": f"{p.first.capitalize()} {p.last.capitalize()}",
                "nbaid": p.nbaid,
            }
            for p in db.session.query(Player).filter(Player.missing).all()
        ]
    )


@player.route("/api/players/missing_nbaid")
def missing_nbaid():
    return jsonify(
        missing=[
            {
                "name": f"{p.first.capitalize()} {p.last.capitalize()}",
                "gid": p.gid,
            }
            for p in db.session.query(Player).filter(Player.nbaid == None).all()
        ]
    )
