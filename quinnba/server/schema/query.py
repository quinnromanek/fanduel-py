import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from datetime import date as Date

from .types import Player
from .training import TrainingRow, training_data

from quinnba import db
from typing import List


class Game(SQLAlchemyObjectType):
    class Meta:
        model = db.Game


class Query(graphene.ObjectType):
    node = relay.Node.Field()

    all_players = SQLAlchemyConnectionField(Player)

    missing_games = graphene.List(graphene.Date, required=True)

    training_input = graphene.List(
        TrainingRow, required=True, season=graphene.Int(required=True)
    )

    missing_players = graphene.List(Player, required=True)

    def resolve_missing_games(_root, info) -> List[graphene.Date]:
        session = info.context["session"]
        db.cache_games(session.bind)
        game_dates = [
            result[0]
            for result in session.query(db.Game.date)
            .filter(~db.Game.player_stats.any())
            .filter(db.Game.date < Date.today())
            .distinct()
            .all()
        ]
        return game_dates

    def resolve_training_input(_root, info, season):
        session = info.context["session"]
        return [
            TrainingRow(**row) for _, row in training_data(season, session).iterrows()
        ]

    def resolve_missing_players(_root, info):
        return Player.get_query(info).filter(db.Player.missing).all()
