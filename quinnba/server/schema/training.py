import graphene
from sqlalchemy.orm import Session
from sqlalchemy import Column, func
from pandas import DataFrame, read_sql
from quinnba.db import PlayerGameStats, TeamGameStats


class TrainingRow(graphene.ObjectType):
    fpts = graphene.Float(required=True)
    avg_min = graphene.Float(required=True)
    avg_reb = graphene.Float(required=True)
    avg_ast = graphene.Float(required=True)
    avg_stl = graphene.Float(required=True)
    avg_blk = graphene.Float(required=True)
    avg_to = graphene.Float(required=True)
    avg_pts = graphene.Float(required=True)


def fpts():
    return (
        PlayerGameStats.pts
        + PlayerGameStats.reb * 1.2
        + PlayerGameStats.stl * 3
        + PlayerGameStats.blk * 3
        - PlayerGameStats.to
        + 1.5 * PlayerGameStats.ast
    ).label("fpts")


def p_avg(col: Column):
    return (
        func.avg(col)
        .over(
            partition_by=PlayerGameStats.gid,
            order_by=PlayerGameStats.date,
            rows=(None, -1),
        )
        .label(f"avg_{col.name}")
    )


def possessions():
    return 0.96 * (
        TeamGameStats.fga
        + TeamGameStats.to
        + 0.44 * TeamGameStats.fta
        - TeamGameStats.oreb
    )


def training_data(season: int, session: Session) -> DataFrame:
    return read_sql(
        session.query(
            fpts(),
            p_avg(PlayerGameStats.min),
            p_avg(PlayerGameStats.reb),
            p_avg(PlayerGameStats.ast),
            p_avg(PlayerGameStats.stl),
            p_avg(PlayerGameStats.blk),
            p_avg(PlayerGameStats.to),
            p_avg(PlayerGameStats.pts),
        )
        .filter(PlayerGameStats.season == season)
        .filter(~PlayerGameStats.out)
        .statement,
        session.bind,
    ).dropna()
