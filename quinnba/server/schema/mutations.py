import graphene
import pandas
from graphene import Mutation, ObjectType, InputObjectType, Field
from os import getenv
import pandas as pd
from .nba import Team, Pos
from .stats import NbaStatLine
from quinnba.db import ingest_game_results, ingest_game_salaries
import quinnba.db as db
from discord_webhook import DiscordWebhook
from .types import Player
from datetime import date as Date


class FanduelPlayer(InputObjectType):
    fanduel_id = graphene.ID(required=True)
    player_name = graphene.String(required=True)
    pos = Pos(required=True)
    secondary_pos = Pos()
    team = Team(required=True)
    opp = Team(required=True)
    salary = graphene.Int(required=True)
    injury_status = graphene.String()


class IngestSalaries(Mutation):
    class Arguments:
        players = graphene.List(FanduelPlayer, required=True)

    count = graphene.Int()

    def mutate(self, info, players):
        df = pandas.DataFrame(players)
        df = df.astype({"fanduel_id": int})
        df["date"] = Date.today()

        ingest_game_salaries(df, info.context["session"])

        return IngestSalaries(count=len(players))


class IngestStats(Mutation):
    class Arguments:
        stats = graphene.List(NbaStatLine, required=True)

    count = graphene.Int()

    def mutate(self, info, stats):
        df = pd.DataFrame(stats)
        df = df.astype({"player_id": int})

        ingest_game_results(df, info.context["session"])
        return IngestStats(count=len(stats))


class NotifyDiscord(Mutation):
    class Arguments:
        message = graphene.String(required=True)
        source = graphene.String(required=True)

    message = graphene.String()

    def mutate(self, info, message, source):
        webhook = DiscordWebhook(url=getenv("DISCORD_WEBHOOK_URL"), content=message)
        webhook.execute()
        return NotifyDiscord(message=message)


class PromoteMissingPlayer(Mutation):
    class Arguments:
        player_ids = graphene.List(graphene.ID, required=True)

    players = graphene.List(Player, required=True)

    def mutate(self, info, player_ids):
        session = info.context["session"]
        Player.get_query(info).filter(db.Player.gid.in_(player_ids)).update(
            {"missing": False}
        )
        session.commit()
        return PromoteMissingPlayer(
            players=Player.get_query(info).filter(db.Player.gid.in_(player_ids)).all()
        )


class CorrectableField(graphene.Enum):
    nbaid = 1
    fanduel_id = 2


class CorrectMissingPlayer(Mutation):
    class Arguments:
        target_id = graphene.ID(required=True)
        source_id = graphene.ID(required=True)
        field = CorrectableField(required=True)

    player = Field(Player, required=True)

    def mutate(self, info, target_id, source_id, field):
        field = CorrectableField.get(field).name
        session = info.context["session"]
        target = Player.get_query(info).filter(db.Player.gid == target_id).one()
        source = Player.get_query(info).filter(db.Player.gid == source_id).one()
        assert not getattr(target, field)
        assert getattr(source, field)
        setattr(target, field, getattr(source, field))
        session.query(db.PlayerGameStats).filter(
            db.PlayerGameStats.gid == source_id
        ).update({"gid": target_id})
        session.query(db.PlayerGameSalary).filter(
            db.PlayerGameSalary.gid == source_id
        ).update({"gid": target_id})
        session.delete(source)
        session.commit()
        return CorrectMissingPlayer(player=target)


class Mutations(ObjectType):
    ingest_salaries = IngestSalaries.Field()
    ingest_nba_stats = IngestStats.Field()
    notify_discord = NotifyDiscord.Field()
    promote_missing_player = PromoteMissingPlayer.Field()
    correct_missing_player = CorrectMissingPlayer.Field()
