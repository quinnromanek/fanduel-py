from graphene import InputObjectType
from .nba import Team
import graphene


class NbaStatLine(InputObjectType):
    # NBA specific info
    player_id = graphene.ID(required=True)
    player_name = graphene.String(required=True)

    game_id = graphene.ID(required=True)

    # Player model
    date = graphene.Date(required=True)
    team = Team(required=True)
    opp = Team(required=True)
    # season
    # pos
    start = graphene.Boolean(required=True)
    inj = graphene.Boolean(required=True)
    out = graphene.Boolean(required=True)

    # TraditionalStatsMixin model
    min = graphene.Float(required=True)
    fgm = graphene.Float(required=True)
    fga = graphene.Float(required=True)
    fg3m = graphene.Float(required=True)
    fg3a = graphene.Float(required=True)
    ftm = graphene.Float(required=True)
    fta = graphene.Float(required=True)
    oreb = graphene.Float(required=True)
    dreb = graphene.Float(required=True)
    reb = graphene.Float(required=True)
    ast = graphene.Float(required=True)
    stl = graphene.Float(required=True)
    blk = graphene.Float(required=True)
    to = graphene.Float(required=True)
    pf = graphene.Float(required=True)
    pts = graphene.Float(required=True)

    # UsageStatsMixin model
    pct_stl = graphene.Float(required=True)
    pct_blka = graphene.Float(required=True)
    pct_ftm = graphene.Float(required=True)
    pct_reb = graphene.Float(required=True)
    pct_pts = graphene.Float(required=True)
    pct_fg3a = graphene.Float(required=True)
    pct_pfd = graphene.Float(required=True)
    pct_fta = graphene.Float(required=True)
    pct_blk = graphene.Float(required=True)
    pct_fga = graphene.Float(required=True)
    usg_pct = graphene.Float(required=True)
    pct_tov = graphene.Float(required=True)
    pct_fg3m = graphene.Float(required=True)
    pct_pf = graphene.Float(required=True)
    pct_dreb = graphene.Float(required=True)
    pct_fgm = graphene.Float(required=True)
    pct_ast = graphene.Float(required=True)
    pct_oreb = graphene.Float(required=True)
