import graphene
from quinnba import nba

Team = graphene.Enum.from_enum(nba.Team)
Pos = graphene.Enum.from_enum(nba.Position)
