from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from quinnba import db
from graphene import relay


class Player(SQLAlchemyObjectType):
    class Meta:
        model = db.Player
        interfaces = (relay.Node,)
