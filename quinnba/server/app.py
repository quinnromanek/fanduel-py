from flask import Flask

from flask_cors import CORS
from quinnba.server.player import player
from quinnba.server.games import games
from quinnba.server.models import models
from quinnba.server.contests import contests
from quinnba.server.database import db, migrate
from quinnba.server.schema import schema
from flask_graphql import GraphQLView
import os


def create_app():
    a = Flask(__name__)
    a.config.from_object("quinnba.server.config.Config")
    db.init_app(a)
    migrate.init_app(a, db)

    return a


app = create_app()
app.register_blueprint(player)
app.register_blueprint(games)
app.register_blueprint(models)
app.register_blueprint(contests)
app.add_url_rule(
    "/api/graphql",
    view_func=GraphQLView.as_view(
        "graphql",
        schema=schema,
        graphiql=True,
        get_context=lambda: {"session": db.session},
    ),
)

CORS(app, resources={r"/*": {"origins": "*"}})


@app.route("/healthz")
@app.route("/api/healthz")
def healthz():
    return "ok"


if __name__ == "__main__":
    host = os.getenv("QUINNBA_HOST", "localhost")
    app.run(host=host, port=5000, debug=True)
