from math import sqrt

from quinnba.nba import fanduel_points
from quinnba.server.database import db
import pandas as pd
from flask import Blueprint, jsonify
from quinnba.db import PlayerModel, PlayerPrediction, PlayerGameStats, Lineup
from typing import Dict
from numpy import nan_to_num

models = Blueprint("models", __name__)


def _gen_stats(df: pd.DataFrame) -> Dict:
    return {
        "count": len(df),
        "me": round(nan_to_num(df["err"].abs().mean()), 3),
        "rmse": round(nan_to_num(df["se"].mean()), 3),
        "skew": round(nan_to_num(df["err"].skew()), 3),
        "kurtosis": round(nan_to_num(df["err"].kurt()), 3),
    }


@models.route("/api/models")
def get_models():
    player_models = db.session.query(PlayerModel)
    models = []

    for model in player_models:

        predictions = pd.read_sql(
            db.session.query(
                PlayerPrediction.pred,
                PlayerPrediction.stddev,
                PlayerPrediction.mid,
                PlayerPrediction.season,
                PlayerGameStats.pts,
                PlayerGameStats.reb,
                PlayerGameStats.ast,
                PlayerGameStats.blk,
                PlayerGameStats.stl,
                PlayerGameStats.to,
            )
            .filter(PlayerPrediction.mid == model.id)
            .join(
                PlayerGameStats,
                (PlayerGameStats.date == PlayerPrediction.date)
                & (PlayerGameStats.gid == PlayerPrediction.gid),
            )
            .statement,
            db.session.bind,
            parse_dates=["date"],
        )

        predictions["fpts"] = fanduel_points(data=predictions)
        predictions["err"] = (predictions["fpts"] - predictions["pred"]) / predictions[
            "stddev"
        ]
        predictions["se"] = predictions["err"] ** 2
        predictions = predictions.fillna(0)

        md = {
            "id": model.id,
            "active": model.active,
            "stats": _gen_stats(predictions),
            "seasons": [
                dict(_gen_stats(pd), season=s)
                for s, pd in predictions.groupby("season")
            ],
        }
        models.append(md)

    return jsonify(models=models)
