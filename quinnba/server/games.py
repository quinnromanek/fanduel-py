from quinnba.nba import fanduel_points
from quinnba.server.database import db
from quinnba.model import most_recent_model
from quinnba.db import (
    Player,
    PlayerGameStats,
    PlayerGameSalary,
    PlayerPrediction,
    Lineup,
    Contest,
    LineupEntry,
    most_recent_salary_date,
)
from sqlalchemy.sql import func
import pandas as pd
import datetime
from numpy import nan

from flask import Blueprint, jsonify, request

games = Blueprint("games", __name__)


@games.route("/api/games/dates")
def get_dates():
    q = (
        db.session.query(PlayerGameSalary.date)
        .order_by(PlayerGameSalary.date.asc())
        .distinct()
        .all()
    )
    return jsonify(dates=[d[0].strftime("%Y-%m-%d") for d in q])


@games.route("/api/games/results/", defaults={"date": None}, methods=["GET", "POST"])
@games.route("/api/games/results/<date>", methods=["GET", "POST"])
def get_results(date):
    if date is None:
        date = most_recent_salary_date(db.session).strftime("%Y-%m-%d")
        if date is None:
            date = datetime.date.today().strftime("%Y-%m-%d")

    if request.method == "POST":
        data = request.json
        for player in data["players"]:
            db.session.query(PlayerGameSalary).filter_by(
                date=date, gid=player["gid"]
            ).one().active = player["active"]
        db.session.commit()

    model = most_recent_model(db.session)

    q = pd.read_sql(
        db.session.query(
            PlayerGameSalary.salary,
            Player.name,
            Player.gid,
            PlayerGameSalary.team,
            PlayerGameSalary.active,
            PlayerGameSalary.pos,
            PlayerGameStats.min,
            PlayerGameStats.pts,
            PlayerGameStats.ast,
            PlayerGameStats.reb,
            PlayerGameStats.blk,
            PlayerGameStats.stl,
            PlayerGameStats.to,
            PlayerPrediction.pred,
        )
        .filter(PlayerGameSalary.date == date)
        .join(Player, Player.gid == PlayerGameSalary.gid)
        .join(
            PlayerGameStats,
            (PlayerGameStats.gid == PlayerGameSalary.gid)
            & (PlayerGameStats.date == PlayerGameSalary.date),
            isouter=True,
        )
        .join(
            PlayerPrediction,
            (PlayerPrediction.gid == PlayerGameSalary.gid)
            & (PlayerPrediction.date == PlayerGameSalary.date)
            & (PlayerPrediction.mid == model.id),
            isouter=True,
        )
        .statement,
        db.session.bind,
    )

    q["fpts"] = fanduel_points("pts", "reb", "ast", "blk", "stl", "to", data=q)
    q = q.round({"fpts": 1, "pred": 1, "min": 1})
    q = q.replace({nan: None})

    return jsonify(date=date, players=q.to_dict(orient="records"))


def lineup_players(lineup_id, model, session):
    q = pd.read_sql(
        session.query(
            LineupEntry.gid,
            LineupEntry.pos,
            Player.name,
            PlayerGameSalary.salary,
            PlayerGameSalary.active,
            PlayerPrediction.pred,
            PlayerGameStats.pts,
            PlayerGameStats.ast,
            PlayerGameStats.reb,
            PlayerGameStats.blk,
            PlayerGameStats.stl,
            PlayerGameStats.to,
        )
        .filter(LineupEntry.lineup_id == lineup_id)
        .join(LineupEntry.lineup)
        .join(LineupEntry.player)
        .join(
            PlayerGameSalary,
            (PlayerGameSalary.gid == LineupEntry.gid)
            & (PlayerGameSalary.date == Lineup.date),
        )
        .join(
            PlayerPrediction,
            (PlayerPrediction.gid == LineupEntry.gid)
            & (PlayerPrediction.date == Lineup.date)
            & (PlayerPrediction.mid == model.id),
        )
        .join(
            PlayerGameStats,
            (PlayerGameStats.gid == LineupEntry.gid)
            & (PlayerGameStats.date == Lineup.date),
            isouter=True,
        )
        .statement,
        session.bind,
    )
    q["fpts"] = fanduel_points("pts", "reb", "ast", "blk", "stl", "to", data=q)
    q = q.replace({nan: None})
    q = q.round({"fpts": 1, "pred": 1})
    q = q.drop(columns=["pts", "reb", "ast", "blk", "stl", "to"])

    return q.to_dict(orient="records")


@games.route("/api/games/lineups/", defaults={"date": None})
@games.route("/api/games/lineups/<date>")
def get_lineups(date):
    if date is None:
        date = most_recent_salary_date(db.session).strftime("%Y-%m-%d")
        if date is None:
            date = datetime.date.today().strftime("%Y-%m-%d")

    model = most_recent_model(db.session)
    lineups = [
        {
            "id": lineup.id,
            "mid": lineup.mid,
            "expected": round(lineup.expected, 1),
            "actual": round(lineup.actual, 1)
            if lineup.actual is not None
            else lineup.actual,
            "contest": {
                "name": contest.name,
                "id": contest.id,
                "cost": contest.cost,
                "entries": contest.entries,
            },
            "players": lineup_players(lineup.id, model, db.session),
            "winnings": contest.winnings(
                (lineup.actual if lineup.actual is not None else 0.0)
            ),
        }
        for lineup, contest in db.session.query(Lineup, Contest)
        .filter(Lineup.mid == model.id)
        .filter(Lineup.date == date)
        .order_by(Lineup.id.desc())
        .join(Lineup.contest)
        .all()
    ]

    return jsonify(lineups=lineups)


@games.route("/api/balance")
def get_balance():
    model = most_recent_model(db.session)

    lineups = pd.read_sql(
        db.session.query(
            Lineup.id,
            Lineup.date,
            Lineup.season,
            Lineup.expected,
            Lineup.actual,
            Lineup.cid,
        )
        .filter(Lineup.mid == model.id)
        .filter(Lineup.actual != None)
        .order_by(Lineup.date.desc())
        .statement,
        db.session.bind,
        parse_dates=["date"],
    )

    contests = {
        c.id: c
        for c in db.session.query(Contest)
        .filter(Contest.id.in_([int(id) for id in lineups["cid"].unique()]))
        .all()
    }

    lineups["cost"] = lineups["cid"].map(lambda id: contests[id].cost)
    lineups["date"] = lineups["date"].map(lambda d: d.strftime("%Y-%m-%d"))
    lineups["won"] = lineups["actual"].combine(
        lineups["cid"], lambda a, c: contests[c].winnings(a)
    )
    lineups["net"] = lineups["won"] - lineups["cost"]
    lineups = lineups.round({"actual": 2, "expected": 2})
    seasons = []
    for season, ls in lineups.groupby("season"):
        agg = ls.sum()
        ls["cumulative"] = (
            ls.sort_values("date", ascending=True)["net"].expanding().sum()
        )

        s = {
            "season": season,
            "balance": agg["won"] - agg["cost"],
            "count": len(ls),
            "percent": round(len(ls[ls["won"] > 0]) / len(ls), 2),
            "lineups": ls[
                ["id", "date", "actual", "cost", "won", "expected", "cumulative", "net"]
            ].to_dict(orient="records"),
        }
        seasons.append(s)
    seasons.sort(key=lambda seas: seas["season"], reverse=True)

    return jsonify(seasons=seasons)
