import os


class Config:
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "postgresql:///quinn")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
