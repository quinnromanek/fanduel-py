from nbapy import scoreboard
from nbapy import game
from typing import List, Optional
import pandas as pd
import datetime
from datetime import date as Date
import logging
from retry import retry
from requests.exceptions import ReadTimeout
import sys
from enum import Enum

FIRST_SEASON = 2016

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
log.addHandler(handler)


def all_seasons(date: Optional[Date] = None) -> List[int]:
    if date is None:
        date = Date.today()

    season = current_season(date)
    return list(range(FIRST_SEASON, season + 1))


def into_season(season):
    base = season % 2000
    return f"20{base - 1}-{base}"


def clean_team(name):
    if name == "nor":
        return "nop"
    if name == "pho":
        return "phx"
    return name


class Team(Enum):
    atl = "atl"
    bos = "bos"
    cha = "cha"
    chi = "chi"
    cle = "cle"
    dal = "dal"
    den = "den"
    det = "det"
    gsw = "gsw"
    hou = "hou"
    ind = "ind"
    lac = "lac"
    lal = "lal"
    mem = "mem"
    mia = "mia"
    mil = "mil"
    min = "min"
    nop = "nop"
    nyk = "nyk"
    bkn = "bkn"
    okc = "okc"
    orl = "orl"
    phi = "phi"
    phx = "phx"
    por = "por"
    sac = "sac"
    sas = "sas"
    tor = "tor"
    uta = "uta"
    was = "was"


class Position(Enum):
    PG = "PG"
    SG = "SG"
    SF = "SF"
    PF = "PF"
    C = "C"


def current_season(date: Optional[Date] = None) -> int:
    if date is None:
        date = Date.today()
    return date.year if date.month <= 8 else date.year + 1


_NUMBER_TO_POSITION = {
    1.0: "PG",
    2.0: "SG",
    3.0: "SF",
    4.0: "PF",
    5.0: "C",
}


def convert_to_pos(pos) -> str:
    try:
        pos = float(pos)
        return _NUMBER_TO_POSITION[pos]
    except ValueError:
        return pos
    except TypeError:
        return pos
    except KeyError:
        return pos


@retry(exceptions=ReadTimeout, tries=3, delay=1, backoff=2, logger=log)
def pull_games_for_date(date: datetime.datetime) -> List[pd.DataFrame]:
    sb = scoreboard.Scoreboard(day=date.day, month=date.month, year=date.year)
    games = sb.game_header()

    games = games.groupby("GAME_ID").first().reset_index()
    return [
        pull_game(date, game_id, into_season(current_season(date)))
        for game_id in games["GAME_ID"]
    ]


@retry(exceptions=ReadTimeout, tries=3, delay=1, backoff=2, logger=log)
def pull_game(date: datetime.datetime, game_id: str, season: str) -> pd.DataFrame:
    stats = game.BoxScore(game_id, season=season).players_stats()
    usage = game.BoxScoreUsage(game_id, season).players_stats()
    usage_stats = list(set(usage.columns) - set(stats.columns))
    usage = usage.reset_index().set_index(["PLAYER_ID"])
    stats = stats.join(usage[usage_stats], on="PLAYER_ID")
    stats = stats.reset_index().set_index(["PLAYER_ID"])
    stats["date"] = date
    stats = stats.reset_index()
    stats = stats.rename(columns=str.lower)
    # print(stats.columns)
    return stats


@retry(exceptions=ReadTimeout, tries=3, delay=1, backoff=2, logger=log)
def schedule(date: datetime) -> pd.DataFrame:
    sb = scoreboard.Scoreboard(day=date.day, month=date.month, year=date.year)
    games = sb.game_header().drop_duplicates(subset=["GAME_ID"])
    games["home"] = games["GAMECODE"].map(lambda gc: gc.split("/")[1][:3])
    games["away"] = games["GAMECODE"].map(lambda gc: gc.split("/")[1][3:])
    games = games[["home", "away"]]
    games["home"] = games["home"].map(str.lower).map(clean_team)
    games["away"] = games["away"].map(str.lower).map(clean_team)
    team = []
    opp = []
    for _, row in games.iterrows():
        team.append(row["home"])
        opp.append(row["away"])
        team.append(row["away"])
        opp.append(row["home"])

    return pd.DataFrame(
        {
            "team": team,
            "opp": opp,
        }
    )


def fanduel_points(
    pts="pts", reb="reb", ast="ast", blk="blk", stl="stl", to="to", data=None
):
    if data is not None:
        pts = data[pts]
        reb = data[reb]
        ast = data[ast]
        blk = data[blk]
        stl = data[stl]
        to = data[to]
    return pts + reb * 1.2 + ast * 1.5 + blk * 3.0 + stl * 3.0 - to
