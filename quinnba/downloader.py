from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
from io import StringIO
import pandas
from os.path import isfile, join
from os import listdir
from quinnba.db import lookup_gid
import datetime
from numpy import nan

from quinnba import nba, util

log = util.setup_logging(__name__)

POS_LIST = ["PG", "SG", "SF", "PF", "C"]


def is_good_response(resp):
    content_type = resp.headers["Content-Type"].lower()
    return (
        resp.status_code == 200
        and content_type is not None
        and (content_type.find("html") > -1 or "xml" in content_type)
    )


def simple_get(url):
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None
    except RequestException as e:
        print("Error during requests to {}: {}".format(url, str(e)))


def _parse_preview_doc(html) -> pandas.DataFrame:
    parsed_html = BeautifulSoup(html, "html.parser")
    for pre in parsed_html.find_all("pre"):
        # print(pre.string)
        for p in pre.find_all("p"):

            if p.text is not None and "GID;" in p.text:
                return pandas.read_csv(StringIO(p.text), delimiter=";")


def _is_active(status: str) -> bool:
    status = status.lower()
    return "out" not in status


def rotoguru_preview(date, session):
    log.info("Pulling preview for %s", date)
    url = f"http://rotoguru1.com/cgi-bin/hstats.cgi?pos=0&sort=4&game=d&colA=0&daypt=0&xavg=3&show=2&fltr=10000000009999901001{date.strftime('%m-%d')}0"
    html = simple_get(url)
    schedule = nba.schedule(date)
    df = _parse_preview_doc(html)
    df = df.rename(columns=str.lower)
    df = df[df["pos"] != 0]
    df = df[df["salary"] != 0]
    df["pos"] = df["pos"].map(nba.convert_to_pos)
    df["team"] = df["team"].map(str.lower).map(nba.clean_team)

    missing = []

    def lookup(nerdid, name):
        gid = lookup_gid(int(nerdid), name, session, "nerdid", missing)
        if gid == nerdid:
            return nan
        return gid

    injuries = nerd_injuries(df["team"].unique())

    log.info("Injury info last updated on %s", injuries["updated_date"].unique()[0])
    injuries["gid"] = injuries["nerdid"].combine(injuries["name"], lookup)
    injuries["active"] = injuries["notes"].map(_is_active)
    injuries = injuries.dropna()

    df = df.join(injuries.set_index("gid")[["active"]], on="gid").fillna(
        value={"active": True}
    )

    df["player_name"] = [" ".join(name.split(", ")[::-1]) for name in df["name"]]
    df = df[["gid", "pos", "player_name", "team", "salary", "active"]]
    df["date"] = date
    df = df.join(schedule.set_index("team"), on="team")
    df = df.drop_duplicates(subset=["gid"])
    return df


def rotoguru_results(date):
    date_str = date.strftime("%Y%m%d")
    url = "http://rotoguru1.com/cgi-bin/hyday.pl?mon={}&day={}&year={}&game=fd&scsv=1".format(
        date.month, date.day, date.year
    )
    print("Scraping %s" % url)
    html = simple_get(url)
    if html is not None:
        parsed_html = BeautifulSoup(html, "html.parser")
        for pre in parsed_html.select("pre"):
            if len(list(pre.children)) == 1:
                csv_text = list(pre.children)[0]
                df = pandas.read_csv(
                    StringIO(csv_text), delimiter=";", parse_dates=["Date"]
                )
                df = df.fillna(value={"Starter": 0, "Stat line": ""})
                # Sometimes rotoguru has improperly unlisted salaries, for now
                # don't worry about that.
                df = df[~df["FD Salary"].isna()]

                # Sometimes DNP is put as minutes, just convert this to 0.  Inactive players have
                # NaN in Minutes
                df = df.replace({"Minutes": "DNP"}, value=0.0)
                df["salary"] = (
                    df[["FD Salary"]].replace("[\$,]", "", regex=True).astype(int)
                )
                df["FDP"] = df["FD Pts"]
                df["start"] = df["Starter"] == 1
                df["Opp"] = [opp.lower() for opp in df["Oppt"]]
                df["Team"] = [opp.lower() for opp in df["Team"]]
                df["active"] = [(0 if s else 1) for s in df["Minutes"].isnull()]
                df["player_name"] = [
                    " ".join(name.split(", ")[::-1]) for name in df["Name"]
                ]
                df = df.rename(columns=str.lower)
                return df[
                    [
                        "gid",
                        "date",
                        "player_name",
                        "start",
                        "pos",
                        "salary",
                        "team",
                        "opp",
                        "active",
                    ]
                ]

    else:
        print("Failed to get Html response")


def nerd_players():
    resp = simple_get("https://www.fantasybasketballnerd.com/service/players")
    rows = []
    players = BeautifulSoup(resp, "xml")
    for player in players.find_all("Player"):
        rows.append([int(player.playerId.text), player.find("name").text])

    return pandas.DataFrame(rows, columns=["nerdid", "player_name"])


def nerd_injuries(sought_teams):
    resp = simple_get("https://www.fantasybasketballnerd.com/service/injuries")
    rows = []
    teams = BeautifulSoup(resp, "xml")
    for team in teams.find_all("Team"):
        if team["code"].lower() not in sought_teams:
            continue
        for player in team.find_all("Player"):
            rows.append(
                [
                    int(player.playerId.text),
                    player.find("name").text,
                    player.injury.text,
                    player.notes.text,
                    datetime.datetime.strptime(player.updated.text, "%Y-%m-%d").date(),
                ]
            )
    return pandas.DataFrame(
        rows, columns=["nerdid", "name", "injury", "notes", "updated_date"]
    )


def undownloaded_dates(max_date):
    path = "./data/2019/results/"

    def dt_from_filename(n):
        ds = n.split(".")[0]
        return pandas.to_datetime(ds)

    file_dates = [
        dt_from_filename(fn) for fn in listdir(path) if isfile(join(path, fn))
    ]
    most_recent = max(file_dates)
    return pandas.date_range(most_recent + pandas.to_timedelta(1, unit="D"), max_date)


def download_results():
    results_to_load = undownloaded_dates(
        pandas.datetime.today() - pandas.to_timedelta(1, unit="D")
    )
    for date in results_to_load:
        rotoguru_results(date)


def main():
    print(rotoguru_preview(datetime.date.today()))


if __name__ == "__main__":
    main()
