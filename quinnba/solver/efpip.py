import pyomo.environ as pyo
import pandas as pd

model = pyo.AbstractModel()

POSITIONS = ["PG", "SG", "SF", "PF", "C"]


model.PG = pyo.Set()
model.SG = pyo.Set()
model.SF = pyo.Set()
model.PF = pyo.Set()
model.C = pyo.Set()

model.A = model.PG | model.SG | model.SF | model.PF | model.C

model.x = pyo.Var(model.A, domain=pyo.Binary)

# Mean of each player
model.u = pyo.Param(model.A)
model.s = pyo.Param(model.A)

model.MaxSalary = pyo.Param(domain=pyo.NonNegativeIntegers)


def obj_expression(m):
    return pyo.summation(m.u, m.x)


model.OBJ = pyo.Objective(rule=obj_expression, sense=pyo.maximize)


model.SalaryConstraint = pyo.Constraint(
    rule=lambda m: pyo.summation(m.s, m.x) <= m.MaxSalary
)


model.PGConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.PG) <= 2)
model.SGConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.SG) <= 2)
model.SFConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.SF) <= 2)
model.PFConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.PF) <= 2)
model.CConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.C) <= 1)


def solve(predictions: pd.DataFrame):
    gids = predictions["gid"].unique()
    assert len(gids) == len(predictions)

    pred_dict = predictions.set_index("gid").to_dict()
    data = {"MaxSalary": {None: 60000}}
    for pos in POSITIONS:
        data[pos] = {None: list(predictions[predictions["pos"] == pos]["gid"])}

    data["s"] = pred_dict["salary"]
    data["u"] = pred_dict["pred"]

    inst = model.create_instance({None: data})
    opt = pyo.SolverFactory("glpk")
    opt.solve(inst)
    selected = [g for g in gids if inst.x[g] == 1]
    return pyo.value(inst.OBJ), sorted(selected)


if __name__ == "__main__":
    data = {
        None: {
            "PG": {None: []},
            "SG": {None: []},
            "SF": {None: []},
            "PF": {None: [1, 2, 3]},
            "C": {None: [4]},
            "MaxSalary": {None: 60000},
            "s": {1: 20000, 2: 15000, 3: 5000, 4: 2000},
            "u": {1: 30.0, 2: 25.0, 3: 15.0, 4: 20.0},
        },
    }
    gids = [1, 2, 3, 4]
    inst = model.create_instance(data)
    print(inst.A.pprint())
    print(inst.C.pprint())
    print(inst.PG.pprint())
    opt = pyo.SolverFactory("glpk")
    opt.solve(inst)
    inst.display()
    print(inst.x.pprint())
    selected = [g for g in gids if inst.x[g] == 1]
    print(selected)
