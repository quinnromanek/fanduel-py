from math import sqrt

import pyomo.environ as pyo
import pandas as pd
from scipy.stats import norm
from quinnba.db import Lineup, LineupEntry, Contest
from quinnba.nba import current_season
from quinnba.util import unique_date

model = pyo.AbstractModel()

POSITIONS = ["PG", "SG", "SF", "PF", "C"]


model.PG = pyo.Set()
model.SG = pyo.Set()
model.SF = pyo.Set()
model.PF = pyo.Set()
model.C = pyo.Set()

model.A = model.PG | model.SG | model.SF | model.PF | model.C

# Index of payout tiers
model.J = pyo.Set()

# Estimated points required to reach each tier in M
model.e = pyo.Param(model.J)

# The payout received for reaching each tier, relative to the previous tier
model.a = pyo.Param(model.J)

# Probability that a team earns ej points for each tier
# Eq 20
model.p = pyo.Var(model.J, domain=pyo.PositiveReals, bounds=(0, 1.0))

# Std deviations from the mean and probabilities
model.K = pyo.RangeSet(-1.0, 4.5, 0.1)


def initialize_z_scores(m):
    raw_z = [norm.sf(k) for k in m.K]
    interval = [i - j for i, j in zip(raw_z[:-1], raw_z[1:])] + [raw_z[-1]]
    return {k: z for k, z in zip(m.K, interval)}


model.z = pyo.Param(model.K, initialize=initialize_z_scores, domain=pyo.PositiveReals)

# Eq 19
model.c = pyo.Var(model.J, model.K, domain=pyo.Binary)


# Eq 18
model.x = pyo.Var(model.A, domain=pyo.Binary)

model.ut = pyo.Var(domain=pyo.Reals)
# Eq 22
model.vt = pyo.Var(domain=pyo.Reals, bounds=(0.0, 20000.0))
# Eq 21
model.ot = pyo.Var(domain=pyo.Reals)

# Average prediction of each player
model.u = pyo.Param(model.A)
# Salary of each player
model.s = pyo.Param(model.A)
# STD deviation of each player
model.o = pyo.Param(model.A)


model.MaxSalary = pyo.Param(domain=pyo.NonNegativeIntegers)


def obj_expression(m):
    return pyo.summation(m.a, m.p)


model.OBJ = pyo.Objective(rule=obj_expression, sense=pyo.maximize)


# Eq 7
model.SalaryConstraint = pyo.Constraint(
    rule=lambda m: pyo.summation(m.s, m.x) <= m.MaxSalary
)

# Team value constraints
# Eq 8
model.MeanConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.u, m.x) == m.ut)

# Eq 9
model.PlayerVarConstraint = pyo.Constraint(
    rule=lambda m: sum((m.o[a] ** 2.0) * m.x[a] for a in m.A) == m.vt
)

# Eq 10-14, 23-25
model.SqrtConstraint = pyo.Piecewise(
    model.ot,
    model.vt,
    pw_pts=list(range(0, 20001, 20)),
    pw_constr_type="EQ",
    pw_repn="CC",
    f_rule=lambda m, v: sqrt(v),
)

M = 500.0

# Eq 15
model.CashConstraint = pyo.Constraint(
    model.J, model.K, rule=lambda m, j, k: m.ut + k * m.ot - m.e[j] <= (M * m.c[j, k])
)
# Eq 16
model.NoCashConstraint = pyo.Constraint(
    model.J,
    model.K,
    rule=lambda m, j, k: -(m.ut + k * m.ot) + m.e[j] - 0.001 <= (M * (1 - m.c[j, k])),
)

# Eq 17
model.ProbConstraint = pyo.Constraint(
    model.J, rule=lambda m, j: sum(m.c[j, k] * m.z[k] for k in m.K) == m.p[j]
)


model.PGConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.PG) <= 2)
model.SGConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.SG) <= 2)
model.SFConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.SF) <= 2)
model.PFConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.PF) <= 2)
model.CConstraint = pyo.Constraint(rule=lambda m: pyo.summation(m.x, index=m.C) <= 1)


def solve(predictions: pd.DataFrame, contest: Contest) -> Lineup:
    gids = predictions["gid"].unique()
    assert len(gids) == len(predictions)

    date = unique_date(predictions["date"])

    mids = predictions["mid"].unique()
    assert len(mids) == 1
    mid = mids[0]

    predictions["salary"] = predictions["salary"] / 60000
    pred_dict = predictions.set_index("gid").to_dict()
    data = {"MaxSalary": {None: 1.0}}
    for pos in POSITIONS:
        data[pos] = {None: list(predictions[predictions["pos"] == pos]["gid"])}

    data["s"] = pred_dict["salary"]
    data["u"] = pred_dict["pred"]
    data["o"] = pred_dict["stddev"]

    data["J"] = range(len(contest.payouts))
    data["e"] = {i: val for i, val in enumerate(contest.thresholds)}
    data["a"] = {i: val / 1000.0 for i, val in enumerate(contest.differential_payouts)}

    inst = model.create_instance({None: data})
    opt = pyo.SolverFactory("glpk")
    result = opt.solve(inst, tee=True)
    print(result)
    print(f"Expected Team Points: {pyo.value(inst.ut)}")
    print(f"Std Deviation: {pyo.value(inst.ot)}")
    selected = [g for g in gids if inst.x[g] == 1]
    filtered = predictions[predictions["gid"].isin(selected)]
    entries = [
        LineupEntry(gid=int(g), pos=p) for g, p in zip(filtered["gid"], filtered["pos"])
    ]

    return Lineup(
        date=date,
        mid=int(mid),
        contest=contest,
        entries=entries,
        expected=pyo.value(inst.ut),
        stddev=pyo.value(inst.ot),
        season=current_season(date),
    )
