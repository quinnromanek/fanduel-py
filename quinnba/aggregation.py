from datetime import date
import pandas as pd
from sqlalchemy import engine
from quinnba.db import (
    Session,
    PlayerGameStats,
    PlayerGameSalary,
    TeamGameStats,
    PlayerAverageStats,
    PlayerSituationalStats,
    TeamAverageStats,
    TeamAverageOppStats,
    PlayerPrediction,
)
import quinnba.util as util
from quinnba.nba import current_season, fanduel_points
from typing import List


def aggregate_players_before_date(slate: pd.DataFrame, eng: engine, s: Session):
    d = util.unique_date(slate["date"])
    season = current_season(d)
    stats = pd.read_sql(
        s.query(PlayerGameStats)
        .filter((PlayerGameStats.gid.in_(slate["gid"])) & (PlayerGameStats.date < d))
        .filter(~PlayerGameStats.out)
        .filter(PlayerGameStats.season == season)
        .order_by(PlayerGameStats.date.asc())
        .statement,
        s.bind,
        parse_dates=["date"],
    )
    if len(stats) == 0:
        return
    stats = stats.drop(columns=["team", "opp", "pos", "start", "inj", "out"])
    averaged = stats.groupby("gid").mean().reset_index()
    averaged["date"] = d
    averaged["season"] = season

    averaged.set_index(["gid", "date"]).to_sql(
        PlayerAverageStats.__tablename__, eng, if_exists="append", index=True
    )

    # This should probably be a transaction
    for gid, date_df in stats.sort_values("date", ascending=False)[
        ["gid", "date"]
    ].groupby("gid"):
        recent = [False for _ in range(PlayerSituationalStats.TRACK_RECENT)]
        for played_date in date_df["date"]:
            days_ago = (d - played_date).days
            if days_ago > PlayerSituationalStats.TRACK_RECENT:
                break
            recent[days_ago - 1] = True
        sit_stats = PlayerSituationalStats(gid=gid, season=season, date=d)
        sit_stats.recent_games = recent
        s.add(sit_stats)
    s.commit()


def aggregate_teams_before_date(slate: pd.DataFrame, eng: engine, s: Session):
    d = util.unique_date(slate["date"])
    season = current_season(d)
    stats = pd.read_sql(
        s.query(TeamGameStats)
        .filter(
            (TeamGameStats.team.in_(slate["team"].unique())) & (TeamGameStats.date < d)
        )
        .filter(TeamGameStats.season == season)
        .order_by(TeamGameStats.date.asc())
        .statement,
        s.bind,
        parse_dates=["date"],
    )
    if len(stats) == 0:
        return
    averaged = stats.drop(columns=["opp"])
    averaged = averaged.groupby("team").mean().reset_index()
    averaged["date"] = d
    averaged["season"] = season
    averaged.set_index(["team", "date"]).to_sql(
        TeamAverageStats.__tablename__, eng, if_exists="append", index=True
    )


def aggregate_opponents_before_date(slate: pd.DataFrame, eng: engine, s: Session):
    d = util.unique_date(slate["date"])
    season = current_season(d)
    stats = pd.read_sql(
        s.query(TeamGameStats)
        .filter(
            (TeamGameStats.opp.in_(slate["team"].unique())) & (TeamGameStats.date < d)
        )
        .filter(TeamGameStats.season == season)
        .order_by(TeamGameStats.date.asc())
        .statement,
        s.bind,
        parse_dates=["date"],
    )
    if len(stats) == 0:
        return

    averaged = stats.drop(columns=["team"])

    averaged = averaged.rename(columns={"opp": "team"})
    averaged = averaged.groupby("team").mean().reset_index()
    averaged["date"] = d
    averaged["season"] = season
    averaged.set_index(["team", "date"]).to_sql(
        TeamAverageOppStats.__tablename__, eng, if_exists="append", index=True
    )


def _build_situation_df(situations):
    create = {
        "date": [s.date for s in situations],
        "gid": [s.gid for s in situations],
    }
    for i in range(PlayerSituationalStats.TRACK_RECENT):
        create[f"game_{i + 1}"] = [int(s.recent_games[i]) for s in situations]
    return pd.DataFrame(create)


def load_slate(date, s: Session):
    return pd.read_sql(
        s.query(PlayerGameSalary)
        .filter(PlayerGameSalary.date == date)
        .filter_by(active=True)
        .statement,
        s.bind,
        parse_dates=["date"],
    )


def load_slate_model_input(slate: pd.DataFrame, s: Session):
    """
    Loads the relevant features for the model to process.

    :param slate: All active players for the given date
    :param s: SQLAlchemy session
    :return: the input slate joined with needed features
    """
    d = util.unique_date(slate["date"])
    season = current_season(d)
    gids = [int(g) for g in slate["gid"].unique()]
    teams = slate["team"].unique()

    player_avgs = pd.read_sql(
        s.query(PlayerAverageStats)
        .filter(PlayerAverageStats.gid.in_(gids))
        .filter(PlayerAverageStats.date == d)
        .filter(PlayerAverageStats.season == season)
        .statement,
        s.bind,
        parse_dates=["date"],
    ).drop(columns=["season"])

    team_opp_avgs = pd.read_sql(
        s.query(TeamAverageOppStats)
        .filter(TeamAverageOppStats.team.in_(teams))
        .filter(TeamAverageOppStats.date == d)
        .filter(TeamAverageOppStats.season == season)
        .statement,
        s.bind,
        parse_dates=["date"],
    ).drop(columns=["season"])

    team_avgs = pd.read_sql(
        s.query(TeamAverageStats.team, TeamAverageStats.date, TeamAverageStats.min)
        .filter(TeamAverageStats.team.in_(teams))
        .filter(TeamAverageStats.date == d)
        .filter(TeamAverageStats.season == season)
        .statement,
        s.bind,
        parse_dates=["date"],
    )

    situations = (
        s.query(PlayerSituationalStats)
        .filter(PlayerSituationalStats.date == d)
        .filter(PlayerSituationalStats.gid.in_(gids))
        .filter(PlayerSituationalStats.season == season)
        .all()
    )

    result = _merge_model_input(
        slate, player_avgs, team_opp_avgs, team_avgs, _build_situation_df(situations)
    )

    result["season"] = season
    return result


def _merge_model_input(
    slate: pd.DataFrame,
    player_avgs: pd.DataFrame,
    team_opp_avgs: pd.DataFrame,
    team_avgs: pd.DataFrame,
    situational: pd.DataFrame,
) -> pd.DataFrame:
    """
    Loads the relevant features for the model to process.

    :param slate: All active players for the given date
    :param player_avgs: Player averages as of a given date. Has index gid, date but no team.
    :return: the input slate joined with needed features
    """

    base = slate[["gid", "date", "team", "opp", "pos"]]

    if len(player_avgs) == 0:
        base = base.iloc[0:0]
        return base
    player_avgs = player_avgs.set_index(["gid", "date"]).rename(
        columns=lambda c: c + "_avg"
    )

    active_team_avgs = (
        player_avgs.join(base[["gid", "date", "team"]].set_index(["gid", "date"]))
        .groupby(["team", "date"])
        .sum()[["min_avg", "usg_pct_avg"]]
    )

    base = base.join(player_avgs, on=["gid", "date"])
    base = base.join(active_team_avgs, on=["team", "date"], rsuffix="_active")

    team_opp_avgs = team_opp_avgs.set_index(["team", "date"]).rename(
        columns=lambda c: c + "_opp"
    )

    base = base.join(team_opp_avgs, on=["opp", "date"])

    team_avgs = team_avgs.set_index(["team", "date"]).rename(
        columns=lambda c: c + "_team_avg"
    )

    base = base.join(team_avgs, on=["team", "date"])

    situational = situational.set_index(["gid", "date"])
    base = base.join(situational, on=["gid", "date"])

    base["min_team_delta"] = base["min_team_avg"] - base["min_avg_active"]
    base["usg_pct_team_delta"] = base["usg_pct_avg"] - base["usg_pct_avg_active"]

    return base.dropna()


def load_training_input(seasons: List[int], points, s: Session) -> pd.DataFrame:
    """Similar to """

    slate = pd.read_sql(
        s.query(PlayerGameSalary)
        .filter(PlayerGameSalary.season.in_(seasons))
        .statement,
        s.bind,
        parse_dates=["date"],
    )

    player_avgs = pd.read_sql(
        s.query(PlayerAverageStats)
        .filter(PlayerAverageStats.season.in_(seasons))
        .statement,
        s.bind,
        parse_dates=["date"],
    ).drop(columns=["season"])

    team_opp_avgs = pd.read_sql(
        s.query(TeamAverageOppStats)
        .filter(TeamAverageOppStats.season.in_(seasons))
        .statement,
        s.bind,
        parse_dates=["date"],
    ).drop(columns=["season"])

    team_avgs = pd.read_sql(
        s.query(TeamAverageStats.team, TeamAverageStats.date, TeamAverageStats.min)
        .filter(TeamAverageStats.season.in_(seasons))
        .statement,
        s.bind,
        parse_dates=["date"],
    )

    situations = (
        s.query(PlayerSituationalStats)
        .filter(PlayerSituationalStats.season.in_(seasons))
        .all()
    )

    merged = _merge_model_input(
        slate, player_avgs, team_opp_avgs, team_avgs, _build_situation_df(situations)
    )

    player_results = (
        pd.read_sql(
            s.query(PlayerGameStats)
            .filter(PlayerGameStats.season.in_(seasons))
            .filter(~PlayerGameStats.out)
            .filter(~PlayerGameStats.inj)
            .statement,
            s.bind,
            parse_dates=["date"],
        ).set_index(["gid", "date"])
        # WARNING: pos is not populated in some test data - if this is
        # ever needed please turn it on.
        .drop(columns=["team", "opp", "pos"])
    )

    merged = merged.join(player_results, on=["gid", "date"])
    merged["fpts"] = points("pts", "reb", "ast", "blk", "stl", "to", data=merged)

    return merged.dropna()


def load_solver_input(slate: pd.DataFrame, mid: int, s: Session) -> pd.DataFrame:
    d = util.unique_date(slate["date"])

    predictions = pd.read_sql(
        s.query(PlayerPrediction)
        .filter(PlayerPrediction.date == d)
        .filter(PlayerPrediction.mid == mid)
        .statement,
        s.bind,
        parse_dates=["date"],
    ).drop(columns=["season", "samples"])

    slate = slate.join(predictions.set_index(["gid", "date"]), on=["gid", "date"])
    # If there were any slate members without predictions, ignore them for now.
    # TODO Use some other value for this rather than throwing away data.
    return slate.dropna()


def load_fpts(slate: pd.DataFrame, s: Session, fpts=fanduel_points):
    d = util.unique_date(slate["date"])

    results = pd.read_sql(
        s.query(
            PlayerGameStats.gid,
            PlayerGameStats.date,
            PlayerGameStats.pts,
            PlayerGameStats.reb,
            PlayerGameStats.ast,
            PlayerGameStats.blk,
            PlayerGameStats.stl,
            PlayerGameStats.to,
        )
        .filter(PlayerGameStats.date == d)
        .statement,
        s.bind,
        parse_dates=["date"],
    )
    slate = slate.join(results.set_index(["gid", "date"]), on=["gid", "date"])
    slate["fpts"] = fpts("pts", "reb", "ast", "blk", "stl", "to", slate)
    return slate.dropna()
