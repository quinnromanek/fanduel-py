import pandas as pd
from typing import Tuple
import logging
import sys


def unique_date(col):
    dates = col.unique()
    assert len(dates) == 1
    return pd.Timestamp(dates[0])


def clean_name(name: str) -> Tuple[str, str]:
    changed_name = name.replace("-", "")
    changed_name = changed_name.replace(".", "")
    changed_name = changed_name.replace("'", "")
    segs = changed_name.lower().split(" ")
    if len(segs) == 1:
        return segs[0], ""
    return segs[0], segs[1]


def setup_logging(name):
    log = logging.getLogger(name)
    log.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    log.addHandler(handler)
    return log
