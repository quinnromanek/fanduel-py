from .base import Base

import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    Float,
    Boolean,
    Date,
    PickleType,
    ForeignKey,
)
from sqlalchemy.orm import relationship

from .stats import TraditionalStatsMixin, UsageStatsMixin


class Player(Base):
    __tablename__ = "players"
    gid = Column(Integer, primary_key=True)

    # Keys for other services
    nbaid = Column(Integer)
    nerdid = Column(Integer)
    fanduel_id = Column(Integer)

    first = Column(String)
    last = Column(String)
    name = Column(String)

    missing = Column(Boolean)


class MissingPlayer(Base):
    __tablename__ = "missingplayers"
    player_id = Column(Integer, ForeignKey(Player.gid), primary_key=True)

    player = relationship("Player")

    first = Column(String)
    last = Column(String)


class PlayerGameSalary(Base):
    __tablename__ = "playergamessalary"

    gid = Column(Integer, primary_key=True)
    date = Column(Date, primary_key=True)
    team = Column(String)
    opp = Column(String)
    season = Column(Integer)
    salary = Column(Integer)
    pos = Column(String)
    active = Column(Boolean, default=True)
    secondary_pos = Column(String)
    injury_status = Column(String)

    game_id = Column(Integer, ForeignKey("games.id"))

    def __repr__(self):
        return f"<PlayerGameSalary({self.gid} on {self.date} ({self.team}) ${self.salary})>"


class PlayerGameStats(Base, TraditionalStatsMixin, UsageStatsMixin):
    __tablename__ = "playergamesstats"

    gid = Column(Integer, primary_key=True)
    date = Column(Date, primary_key=True)

    # Semantic Information
    team = Column(String)
    opp = Column(String)
    season = Column(Integer)
    pos = Column(String)
    start = Column(Boolean)
    inj = Column(Boolean)
    out = Column(Boolean)

    game_id = Column(Integer, ForeignKey("games.id"))


class PlayerAverageStats(Base, TraditionalStatsMixin, UsageStatsMixin):
    __tablename__ = "playeraveragestats"
    gid = Column(Integer, primary_key=True)
    date = Column(Date, primary_key=True)
    season = Column(Integer)


class PlayerSituationalStats(Base):
    __tablename__ = "playersituationalstats"
    gid = Column(Integer, primary_key=True)
    date = Column(Date, primary_key=True)
    season = Column(Integer)

    recent_stored = Column(Integer)

    TRACK_RECENT = 5

    @property
    def recent_games(self):
        """Whether games were played on the 5 past nights"""
        return [
            bool(self.recent_stored & (1 << shift))
            for shift in reversed(range(PlayerSituationalStats.TRACK_RECENT))
        ]

    @recent_games.setter
    def recent_games(self, value):
        assert len(value) == PlayerSituationalStats.TRACK_RECENT
        self.recent_stored = 0
        for i, value in enumerate(reversed(value)):
            if value:
                self.recent_stored |= 1 << i


class PlayerModel(Base):
    __tablename__ = "models"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=datetime.datetime.now)
    model = Column(PickleType(protocol=4))
    """A list of seasons."""
    seasons_hash = Column(String)
    features_hash = Column(String)
    neg_mean_squared_error = Column(Float)
    samples = Column(Integer)
    equation = Column(String)
    active = Column(Boolean, default=False)


class PlayerPrediction(Base):
    __tablename__ = "playerpredictions"
    gid = Column(Integer, primary_key=True)
    date = Column(Date, primary_key=True)
    mid = Column(Integer, ForeignKey("models.id", ondelete="CASCADE"), primary_key=True)
    season = Column(Integer)

    pred = Column(Float)
    stddev = Column(Float)
    var = Column(Float)
    samples = Column(Float)
