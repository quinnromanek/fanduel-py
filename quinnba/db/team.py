from .base import Base
from .stats import TraditionalStatsMixin, UsageStatsMixin
from sqlalchemy import Column, String, Date, Integer, ForeignKey


class TeamGameStats(Base, TraditionalStatsMixin, UsageStatsMixin):
    __tablename__ = "teamgamesstats"
    team = Column(String, primary_key=True)
    date = Column(Date, primary_key=True)

    season = Column(Integer)
    opp = Column(String)

    game_id = Column(Integer, ForeignKey("games.id"))


class TeamAverageStats(Base, TraditionalStatsMixin, UsageStatsMixin):
    __tablename__ = "teamaveragestats"
    team = Column(String, primary_key=True)
    date = Column(Date, primary_key=True)
    season = Column(Integer)


class TeamAverageOppStats(Base, TraditionalStatsMixin, UsageStatsMixin):
    __tablename__ = "teamaverageoppstats"
    team = Column(String, primary_key=True)
    date = Column(Date, primary_key=True)
    season = Column(Integer)
