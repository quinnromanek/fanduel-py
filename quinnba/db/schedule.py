from .base import Base
from sqlalchemy import Column, String, Date, Integer, Enum
from sqlalchemy.engine import Engine, Connection
from sqlalchemy.orm import Session, relationship
from pandas import DataFrame, read_sql
import requests
from typing import Any, Dict, Union, List
from quinnba.nba import clean_team, all_seasons, current_season

from quinnba.util import setup_logging

log = setup_logging(__name__)


class Game(Base):
    __tablename__ = "games"
    id = Column(Integer, primary_key=True, unique=True)
    date = Column(Date)

    home = Column(String)
    away = Column(String)
    season = Column(Integer)

    salaries = relationship("PlayerGameSalary", backref="game")
    player_stats = relationship("PlayerGameStats", backref="game")
    team_stats = relationship("TeamGameStats", backref="game")


def _extract_game(g: Dict[str, Any]) -> Dict[str, Union[str, int]]:
    return {
        "id": int(g["gid"]),
        "date": g["gdte"],
        "home": clean_team(g["h"]["ta"].lower()),
        "away": clean_team(g["v"]["ta"].lower()),
        "status": g["stt"],
    }


def _record_game(game_id: str) -> bool:
    return game_id.startswith("002")


def _reference_schedule(season: int) -> DataFrame:
    r = requests.get(
        f"https://data.nba.com/data/10s/v2015/json/mobile_teams/nba/{season - 1}/league/00_full_schedule.json"
    )
    json = r.json()
    game_hashes = [
        _extract_game(g)
        for mscd in json["lscd"]
        for g in mscd["mscd"]["g"]
        if _record_game(g["gid"])
    ]

    games = DataFrame(game_hashes)
    games["season"] = season
    return games


def _loaded_seasons(eng: Union[Engine, Connection]) -> List[int]:
    with Session(eng) as session:
        return session.query(Game.season).distinct().all()


def cache_games(eng: Union[Engine, Connection]):
    loaded = set(ls[0] for ls in _loaded_seasons(eng))
    seasons_to_pull = (set(all_seasons()) - loaded).union({current_season()})
    log.info(seasons_to_pull)
    for season in seasons_to_pull:
        _ingest_reference_schedule(season, eng)


def _ingest_reference_schedule(season: int, eng: Union[Engine, Connection]):
    df = _reference_schedule(season)
    session = Session(eng)
    existing = read_sql(
        session.query(Game.id).filter(Game.season == season).statement, session.bind
    )
    df = df[df["status"] != "PPD"]
    df = df[~df["id"].isin(existing["id"])]
    df.set_index(["id"])[["date", "home", "away", "season"]].to_sql(
        Game.__tablename__, eng, if_exists="append", method="multi"
    )


if __name__ == "__main__":
    sched = _reference_schedule(2022)
    print(sched)
