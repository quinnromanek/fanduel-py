from sqlalchemy import Column, Float


class TraditionalStatsMixin:
    # Traditional Stats
    min = Column(Float)
    fgm = Column(Float)
    fga = Column(Float)
    fg3m = Column(Float)
    fg3a = Column(Float)
    ftm = Column(Float)
    fta = Column(Float)
    oreb = Column(Float)
    dreb = Column(Float)
    reb = Column(Float)
    ast = Column(Float)
    stl = Column(Float)
    blk = Column(Float)
    to = Column(Float)
    pf = Column(Float)
    pts = Column(Float)


class UsageStatsMixin:
    # Usage Stats
    pct_stl = Column(Float)
    pct_blka = Column(Float)
    pct_ftm = Column(Float)
    pct_reb = Column(Float)
    pct_pts = Column(Float)
    pct_fg3a = Column(Float)
    pct_pfd = Column(Float)
    pct_fta = Column(Float)
    pct_blk = Column(Float)
    pct_fga = Column(Float)
    usg_pct = Column(Float)
    pct_tov = Column(Float)
    pct_fg3m = Column(Float)
    pct_pf = Column(Float)
    pct_dreb = Column(Float)
    pct_fgm = Column(Float)
    pct_ast = Column(Float)
    pct_oreb = Column(Float)
