from sqlalchemy import create_engine, engine, or_, and_, case
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker
import pandas as pd
from typing import List, Optional

import quinnba.util as util
from quinnba.nba import current_season, clean_team

from .base import Base

from .player import *
from .team import *
from .contest import *
from .schedule import *

log = util.setup_logging(__name__)


Session = sessionmaker()


class DbError(RuntimeError):
    pass


def connect(addr: str, drop=False, eng: Optional[Engine] = None, create=True) -> Engine:
    if eng is None:
        eng = create_engine(addr)
        if eng is None:
            raise DbError("failed to create engine")
    if drop:
        log.warning("Dropping all tables!")
        Base.metadata.drop_all(eng)
    if create:
        Base.metadata.create_all(eng)
    Session.configure(bind=eng)
    return eng


def map_players(input: pd.DataFrame, key: str, s: Session) -> pd.DataFrame:
    existing_ids = pd.read_sql(
        s.query(Player.gid, getattr(Player, key))
        .filter(getattr(Player, key).in_(input[key]))
        .statement,
        s.bind,
    ).astype({"gid": int})
    input = input.join(
        existing_ids.set_index(key),
        on=key,
    )
    existing_fixed = input.dropna()[["gid", key]]
    input = input[input["gid"].isna()].drop(columns=["gid"])

    if len(input) == 0:
        return existing_fixed
    names = [util.clean_name(name) for name in input["player_name"]]
    input["first"] = [n[0] for n in names]
    input["last"] = [n[1] for n in names]
    matching_names = pd.read_sql(
        s.query(Player.gid, Player.first, Player.last)
        .filter(getattr(Player, key) == None)
        .filter(
            or_(
                and_(Player.first == first, Player.last == last)
                for first, last in names
            )
        )
        .statement,
        s.bind,
    ).astype({"gid": int})
    input = pd.merge(input, matching_names, how="left", on=["first", "last"])

    # Update foreign keys
    name_fixed = input.dropna()
    if len(name_fixed) > 0:
        payload = dict(zip(name_fixed.gid.astype(int), name_fixed[key]))
        s.query(Player).filter(Player.gid.in_(input["gid"])).update(
            {getattr(Player, key): case(payload, value=Player.gid)},
            synchronize_session=False,
        )
        s.commit()
    input = input[input["gid"].isna()].drop(columns=["gid"])
    name_fixed = name_fixed[[key, "gid"]]

    if len(input) == 0:
        return pd.concat([existing_fixed, name_fixed])

    new_players = input.rename(columns={"player_name": "name"})
    new_players["missing"] = True

    new_players.to_sql(Player.__tablename__, s.bind, if_exists="append", index=False)

    new_gids = pd.read_sql(
        s.query(Player.gid, getattr(Player, key))
        .filter(getattr(Player, key).in_(new_players[key]))
        .statement,
        s.bind,
    ).astype({"gid": int})

    result = pd.concat([existing_fixed, name_fixed, new_gids])
    return result


def lookup_gid(
    foreign_id: int, name: str, session: Session, attr: str, fixed=None, missing=None
) -> int:
    by_id = (
        session.query(Player).filter(getattr(Player, attr) == foreign_id).one_or_none()
    )
    if by_id:
        return by_id.gid

    first, last = util.clean_name(name)
    by_full_name = session.query(Player).filter_by(first=first, last=last).one_or_none()
    if by_full_name:
        setattr(by_full_name, attr, foreign_id)
        if fixed is not None:
            fixed.append((by_full_name.gid, by_full_name.nbaid))
        session.commit()
        return by_full_name.gid

    log.error(
        "Failed to find Player entry for %s %s (%s=%d): no matching players found",
        first,
        last,
        attr,
        foreign_id,
    )
    if missing is not None:
        missing.append((foreign_id, first, last))
    return foreign_id


def fix_gid(current_gid: int, new_gid: int, s: Session):
    entries = s.query(PlayerGameStats).filter_by(gid=current_gid).all()
    for e in entries:
        e.gid = new_gid
        salary = s.query(PlayerGameSalary).filter_by(gid=new_gid, date=e.date).first()
        if salary is not None:
            e.pos = salary.pos

    s.query(MissingPlayer).filter_by(nbaid=current_gid).delete()

    player = s.query(Player).filter_by(gid=new_gid).first()
    if player:
        player.nbaid = current_gid

    s.commit()


def ingest_players(
    players: pd.DataFrame, session: Session, extra: List[str] = [], missing=False
):
    """
    Creates players if they don't already exist.
    :param players: dataframe with columns "gid" and "player_name"
    """
    for _, row in players.iterrows():
        if not session.query(Player).filter_by(gid=row["gid"]).one_or_none():
            first, last = util.clean_name(row["player_name"])
            log.info("Adding new player %s", row["player_name"])
            session.add(
                Player(gid=row["gid"], first=first, last=last, name=row["player_name"])
            )


def ingest_game_salaries(salaries: pd.DataFrame, session: Session):
    """
    Adds Salaries to DB.  Should only receive salaries of active players.
    :param salaries: dataframe requiring columns "gid", "date", "team", "pos"
      "salary", "opp, "player_name"
    :param eng:
    :return:
    """
    d = util.unique_date(salaries["date"])
    season = current_season(d)

    mapping_input = salaries[["player_name", "fanduel_id"]]
    gids = map_players(mapping_input, "fanduel_id", session)

    # Add gid column
    salaries = salaries.join(gids.set_index("fanduel_id"), on="fanduel_id")
    games = pd.read_sql(
        session.query(Game.id.label("game_id"), Game.home, Game.away)
        .filter(Game.date == d)
        .statement,
        session.bind,
    )
    games = pd.concat(
        [
            games[["game_id", "home"]].rename(columns={"home": "team"}),
            games[["game_id", "away"]].rename(columns={"away": "team"}),
        ]
    )

    salaries["season"] = season
    salaries["team"] = salaries["team"].map(str.lower).map(clean_team)
    salaries["opp"] = salaries["opp"].map(str.lower).map(clean_team)
    salaries["active"] = salaries["injury_status"].isna()
    salaries = salaries.join(games.set_index("team"), on="team")

    salaries[
        [
            "gid",
            "date",
            "team",
            "opp",
            "season",
            "salary",
            "pos",
            "active",
            "secondary_pos",
            "injury_status",
            "game_id",
        ]
    ].set_index(["gid", "date"]).to_sql(
        PlayerGameSalary.__tablename__,
        session.bind,
        if_exists="append",
        index=True,
    )


def _other_team(options):
    def other(team):
        if team == options[0]:
            return options[1]
        return options[0]

    return other


def _injured(comment: str) -> bool:
    if type(comment) is not str:
        return False
    comment = comment.lower()
    return (
        len(comment) > 0
        and "dnp" not in comment
        and "rest" not in comment
        and "susp" not in comment
    )


def _out(comment: str) -> bool:
    if type(comment) is not str:
        return False
    comment = comment.lower()
    return len(comment) > 0 and "dnp" not in comment


def _minutes(min_str: str):
    if type(min_str) is not str:
        return 0.0

    min, sec = min_str.split(":")
    return float(min) + (float(sec) / 60)


# Dataframe comes from NbaStatLine
def ingest_game_results(all_results: pd.DataFrame, s: Session):

    d = util.unique_date(all_results["date"])
    all_results["season"] = current_season(pd.Timestamp(d))
    all_results = all_results.rename(columns={"player_id": "nbaid"})

    mapping_input = all_results[["player_name", "nbaid"]]
    gids = map_players(mapping_input, "nbaid", s)

    # Add gid column
    all_results = all_results.join(gids.set_index("nbaid"), on="nbaid")

    # assert len(all_results["gid"]) == len(all_results["gid"].unique())

    salaries = pd.read_sql(
        s.query(PlayerGameSalary.gid, PlayerGameSalary.pos)
        .filter(PlayerGameSalary.date == d)
        .statement,
        s.bind,
    )
    all_results = all_results.join(salaries.set_index("gid"), on="gid")

    all_results = all_results.drop(
        columns=[
            "nbaid",
            "player_name",
        ]
    )

    fill_cols = set(all_results.columns) - {
        "gid",
        "date",
        "team",
        "opp",
        "season",
        "pos",
        "start",
        "inj",
        "out",
    }

    all_results = all_results.fillna(value={col: 0.0 for col in fill_cols})

    team_results = all_results.drop(columns=["pos", "inj", "start", "out", "gid"])
    opps = team_results[["team", "opp"]].groupby("team").first()
    team_results = team_results.groupby("team").sum()
    team_results = team_results.join(opps)
    team_results["date"] = d
    team_results["season"] = current_season(pd.Timestamp(d))

    with s.begin():
        team_results.reset_index().set_index(["team", "date"]).to_sql(
            TeamGameStats.__tablename__, s.bind, if_exists="append", index=True
        )

        all_results = all_results.set_index(["gid", "date"])
        all_results.to_sql(
            PlayerGameStats.__tablename__,
            s.bind,
            if_exists="append",
            index=True,
        )


def most_recent_stat_date(season, s: Session):
    result = (
        s.query(PlayerGameStats.date)
        .order_by(PlayerGameStats.date.desc())
        .filter(PlayerGameStats.season == season)
        .first()
    )
    if result:
        return result[0]
    return result


def most_recent_salary_date(s: Session, season=None):
    query = s.query(PlayerGameSalary.date).order_by(PlayerGameSalary.date.desc())
    if season is not None:
        query = query.filter(PlayerGameSalary.season == season)

    result = query.first()

    if result:
        return result[0]
    return result


def slate_loaded(date, session=None) -> bool:
    session = session or Session()
    return (
        session.query(PlayerGameSalary.date)
        .filter(PlayerGameSalary.date == date)
        .first()
        is not None
    )


def contests_for_day(date: datetime.date, session):
    return (
        session.query(Contest)
        .filter(Contest.enabled == True)
        .join(Contest.days)
        .filter(ContestDay.day == date.weekday())
        .all()
    )
