from sqlalchemy.orm import relationship, validates
from math import log

from .base import Base

from sqlalchemy import (
    Column,
    Integer,
    Date,
    Float,
    ForeignKey,
    PickleType,
    Boolean,
    String,
)


class Lineup(Base):
    __tablename__ = "lineups"
    id = Column(Integer, primary_key=True)

    date = Column(Date)
    season = Column(Integer)
    mid = Column(Integer)

    cid = Column(Integer, ForeignKey("contests.id"))
    contest = relationship("Contest")
    expected = Column(Float)
    stddev = Column(Float)
    actual = Column(Float)
    entries = relationship(
        "LineupEntry", back_populates="lineup", cascade="all, delete"
    )

    @property
    def players(self):
        return [e.gid for e in self.entries]


class LineupEntry(Base):
    __tablename__ = "lineupentries"
    id = Column(Integer, primary_key=True)

    lineup_id = Column(Integer, ForeignKey("lineups.id", ondelete="CASCADE"))
    lineup = relationship("Lineup", back_populates="entries")

    gid = Column(Integer, ForeignKey("players.gid"))
    player = relationship("Player")

    pos = Column(String)


def _contest_rank(r, entries):
    if r < 1.0:
        return r
    return r / entries


def expected_fpts(rank: float) -> float:
    return 278 - 9.01 * log(rank)


class Contest(Base):
    __tablename__ = "contests"
    id = Column(Integer, primary_key=True)

    name = Column(String)
    entries = Column(Integer)
    cost = Column(Float)
    # List of (Rank, Payout), sorted by ascending payout.
    tiers = Column(PickleType(protocol=4))
    enabled = Column(Boolean, default=True)
    days = relationship("ContestDay", back_populates="contest", cascade="all, delete")

    @validates("tiers")
    def validate_tiers(self, _key, tiers):
        return sorted(tiers, key=lambda t: t[1])

    @property
    def thresholds(self):
        return [expected_fpts(_contest_rank(t[0], self.entries)) for t in self.tiers]

    @property
    def payouts(self):
        return [t[1] for t in self.tiers]

    @property
    def differential_payouts(self):
        return [self.payouts[0]] + [
            i - j for i, j in zip(self.payouts[1:], self.payouts[:-1])
        ]

    def winnings(self, score: float) -> float:
        total = 0.0
        for threshold, payout in zip(self.thresholds, self.differential_payouts):
            if score >= threshold:
                total += payout
        return total


class ContestDay(Base):
    __tablename__ = "contestdays"
    id = Column(Integer, primary_key=True)
    contest_id = Column(Integer, ForeignKey("contests.id", ondelete="CASCADE"))
    contest = relationship("Contest", back_populates="days")
    # Day of week, 0 is monday, 6 is Sunday
    day = Column(Integer)
