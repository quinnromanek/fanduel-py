# import matplotlib.pyplot as plt
from sqlalchemy import func

from quinnba.db import *
from quinnba.cron import *
import pandas as pd
from dotenv import load_dotenv

# import seaborn as sns
# from scipy.stats import trim_mean

from quinnba.nba import fanduel_points, current_season, FIRST_SEASON
from datetime import date as Date


def plot_resids(session, eng):
    predictions = pd.read_sql(
        session.query(
            PlayerPrediction.pred,
            PlayerPrediction.stddev,
            PlayerPrediction.mid,
            PlayerPrediction.season,
            PlayerGameStats.pts,
            PlayerGameStats.reb,
            PlayerGameStats.ast,
            PlayerGameStats.blk,
            PlayerGameStats.stl,
            PlayerGameStats.to,
        )
        .filter(PlayerPrediction.mid == 5)
        .join(
            PlayerGameStats,
            (PlayerGameStats.date == PlayerPrediction.date)
            & (PlayerGameStats.gid == PlayerPrediction.gid),
        )
        .statement,
        session.bind,
        parse_dates=["date"],
    )
    predictions["fpts"] = fanduel_points(data=predictions)
    predictions["error"] = predictions["fpts"] - predictions["pred"]

    sns.scatterplot(data=predictions, x="pred", y="error")
    plt.show()


def plot_player(session):
    fig, axes = plt.subplots(1, 3)

    gid = (
        session.query(PlayerGameStats)
        .filter(PlayerGameStats.season == 2016)
        .order_by(func.random())
        .first()
        .gid
    )
    player = session.query(Player).filter_by(gid=gid).first()
    print(f"{gid}: {player.name}")
    stats = pd.read_sql(
        session.query(PlayerGameStats.pts, PlayerGameStats.reb, PlayerGameStats.ast)
        .filter_by(gid=gid, season=2016)
        .statement,
        session.bind,
    )

    print(stats.mean())
    print(stats.apply(lambda s: trim_mean(s, 0.2)))
    sns.histplot(ax=axes[0], data=stats, x="pts")
    sns.histplot(ax=axes[1], data=stats, x="reb")
    sns.histplot(ax=axes[2], data=stats, x="ast")
    plt.show()


def main():
    load_dotenv()
    db_url = os.getenv("DB_URL")
    if not db_url:
        raise ValueError("DB_URL must be defined")
    eng = db.connect(db_url, drop=False, create=False)
    session = Session()
    attrs = ["reb", "ast", "stl", "blk", "to", "pts"]
    cols = [
        func.avg(getattr(PlayerGameStats, attr))
        .over(
            partition_by=PlayerGameStats.gid,
            order_by=PlayerGameStats.date,
            rows=(None, -1),
        )
        .label(f"avg_{attr}")
        for attr in attrs
    ]
    stats = pd.read_sql(
        session.query(
            PlayerGameStats.gid,
            PlayerGameStats.date,
            fpts,
            *cols,
        ).filter(TeamGameStats.season == 2019)
        # .filter(PlayerGameStats.gid == 3541)
        .statement,
        session.bind,
    )
    print(stats)


if __name__ == "__main__":
    main()
