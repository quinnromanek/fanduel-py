# Daily schedule
# Pull slate and aggregate data

# Run model and solve linueps
# Pull data

# Model creation
# Pull training data
# Train model
# Save model
import datetime


from quinnba import nba
from quinnba.downloader import rotoguru_results
from quinnba.util import unique_date
from quinnba.nba import pull_games_for_date, current_season
import quinnba.model
import quinnba.db as db
import quinnba.aggregation as agg
from datetime import timedelta
import pandas as pd
import logging
import sys
import os

# import requests_cache
from quinnba.solver import mmip

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
log.addHandler(handler)


def pull_games(date, eng, session: db.Session):
    games = pull_games_for_date(date)
    if len(games) == 0 or sum(len(g) for g in games) == 0:
        log.info("No games found on %s", date)
    else:
        log.info("Loading %d games from %s", len(games), date)
        db.ingest_game_results(games, eng, session)


def base_stats_pull_date(today, session):
    """
    The most recent date we haven't pulled stats from.
    If no stats have been pulled for this season, just return yesterday's date.

    :param today: current date
    :return: date to start from
    """
    season = current_season(today)

    last_date = db.most_recent_stat_date(season, session)
    if last_date:
        return last_date + timedelta(days=1)
    return today - timedelta(days=1)


# Airflow Task
def pull_latest_results(current_date, eng, session):
    pull_start_date = base_stats_pull_date(current_date, session)

    while pull_start_date <= current_date:
        pull_games(pull_start_date, eng, session)
        pull_start_date += timedelta(days=1)


def ingest_slate(slate, current_date, eng, session):
    if db.slate_loaded(current_date, session):
        log.info("Slate %s already ingested.", current_date)
    else:
        log.info("Ingesting salaries for slate %s", current_date)
        assert len(slate["gid"]) == len(slate["gid"].unique())
        db.ingest_players(slate, session)
        db.ingest_game_salaries(slate, eng)

        agg.aggregate_players_before_date(slate, eng, session)
        agg.aggregate_teams_before_date(slate, eng, session)
        agg.aggregate_opponents_before_date(slate, eng, session)


def daily_update(slate, eng, session):
    current_date = unique_date(slate["date"])

    log.info("Starting daily update for %s", current_date)

    pull_latest_results(current_date - datetime.timedelta(days=1), eng, session)

    ingest_slate(slate, current_date, eng, session)


# TODO handle active ingestion for 2021 Playoffs
# 2020 End of reg season (before bubble): 2020-03-12
# 2019 Playoffs: 2019-04-13
# 2018 Playoffs: 2018-04-14
# 2017 Playoffs: 2017-04-15
# 2016 Playoffs: 2016-04-16
def ingest_historical_data(slates: pd.DataFrame, playoffs, eng, session):
    slates = slates.rename(columns=str.lower)
    slates = slates.sort_values("date")
    slates = slates[slates["date"] < pd.to_datetime(playoffs)]
    slates["player_name"] = slates["first last"]
    slates["salary"] = slates["fd sal"]
    slates["pos"] = slates["fd pos"].map(nba.convert_to_pos)
    slates = (
        slates[["gid", "date", "player_name", "salary", "pos", "team", "opp", "active"]]
        .sort_values("date")
        .dropna()
    )

    i = 0
    for date, slate in slates.groupby("date"):
        daily_update(slate, eng, session)
        i += 1
        # if i > 20:
        #     return


def ingest_yesterdays_data(today, eng, session):
    yesterday = today - datetime.timedelta(days=1)
    results = rotoguru_results(yesterday)

    ingest_slate(results, yesterday, eng, session)

    pull_latest_results(yesterday, eng, session)


def predict_slate(current_date, session, eng, player_model=None):

    if player_model is None:
        player_model = quinnba.model.most_recent_model(session)

    if quinnba.model.already_predicted(current_date, player_model, session):
        log.info(f"Already made predictions for {current_date}")
    else:
        log.info(f"Predicting {current_date}")
        slate = agg.load_slate(current_date, session)
        input = agg.load_slate_model_input(slate, session)
        if len(input) == 0:
            log.info(f"No results for {current_date}")
            return
        predictions = quinnba.model.predict(player_model, input)
        quinnba.model.store_predictions(predictions, eng)


def solve_predicted_slate(
    date: datetime.date,
    session: db.Session,
    model=None,
    check=True,
    contests=None,
):
    # Check if lineups already exist for idempotence
    if model is None:
        model = quinnba.model.most_recent_model(session)
    if check and (
        session.query(db.Lineup)
        .filter(db.Lineup.date == date)
        .filter(db.Lineup.mid == model.id)
        .first()
        is not None
    ):
        log.info("Lineups for model %d on date %s are already present.", model.id, date)
    else:
        log.info("Solving lineup for model %d on date %s.", model.id, date)
        slate = agg.load_slate(date, session)
        preds = agg.load_solver_input(slate, model.id, session)
        if len(preds) == 0:
            log.info("No predictions for model %d on date %s.", model.id, date)
            return

        if contests is None:
            contests = db.contests_for_day(date, session)
        for contest in contests:
            lineup = mmip.solve(preds, contest)
            session.add(lineup)
        session.commit()


def update_lineup_results(session):
    unset = session.query(db.Lineup).filter(db.Lineup.actual == None).all()
    for lineup in unset:
        slate = agg.load_slate(lineup.date, session)
        results = agg.load_fpts(slate, session)
        if len(results) == 0:
            log.info("Results for %s have not been downloaded yet", lineup.date)
            continue
        log.info("Updating Lineup results for %s", lineup.date)
        slate = slate.join(
            results.set_index(["gid", "date"])[["fpts"]], on=["gid", "date"]
        )
        lineup.actual = slate[slate["gid"].isin(lineup.players)]["fpts"].sum()
    session.commit()


WEEKEND_CONTEST = db.Contest(
    id=2,
    name="Weekend Clutch Shot",
    cost=4.44,
    entries=107250,
    tiers=[
        (1, 100000),
        (2, 30000),
        (3, 20000),
        (4, 10000),
        (5, 5000),
        (6, 3000),
        (7, 2000),
        (8, 1000),
        (9, 750),
        (10, 500),
        (12, 400),
        (15, 300),
        (19, 250),
        (25, 200),
        (35, 150),
        (50, 100),
        (70, 75),
        (160, 40),
        (230, 30),
        (445, 20),
        (620, 18),
        (870, 16),
        (1270, 15),
        (1770, 14),
        (2370, 13),
        (3170, 12),
        (4170, 11),
        (6165, 10),
        (13210, 8),
        (24730, 7),
    ],
    days=[
        db.ContestDay(day=5),
        db.ContestDay(day=6),
    ],
)

WEEKDAY_CONTEST = db.Contest(
    id=3,
    name="Weekday Clutch Shot",
    cost=3.0,
    entries=198000,
    tiers=[
        (1, 100000),
        (2, 30000),
        (3, 20000),
        (4, 10000),
        (5, 6000),
        (7, 3000),
        (8, 2000),
        (10, 1500),
        (12, 1000),
        (14, 750),
        (17, 500),
        (22, 400),
        (30, 300),
        (40, 200),
        (50, 150),
        (60, 100),
        (75, 80),
        (100, 60),
        (150, 50),
        (200, 40),
        (400, 30),
        (600, 25),
        (800, 20),
        (1000, 15),
        (1500, 12),
        (3000, 10),
        (5000, 8),
        (10000, 6),
        (53080, 5),
    ],
    days=[db.ContestDay(day=i) for i in range(5)],
)


def main():
    db_url = os.getenv("DB_URL")
    if not db_url:
        raise ValueError("DB_URL must be defined")
    conn = db.connect(db_url, drop=False, create=False)
    session = db.Session()

    # train = agg.load_training_input([2016], nba.fanduel_points, session)
    # x, y, _ = quinnba.model.extract_model_data(train)
    #
    # pt = PowerTransformer()
    # pt.fit(x)
    # x = pt.transform(x)
    #
    #
    #
    #
    #
    # pd.DataFrame(x).hist()
    # plt.show()

    # solve_predicted_slate(datetime.date.today(), session)

    # ingest_historical_data(
    #     pd.read_csv(
    #         "../fanduel-data/rotoguru/nba-dhd-2020.csv",
    #         parse_dates=["Date"],
    #         delimiter=":",
    #     ),
    #     "2020-03-12",
    #     conn,
    #     session,
    # )

    # today = datetime.date.today() - datetime.timedelta(days=1)
    # ingest_yesterdays_data(today, conn, session)

    # quinnba.model.train_new_model([2016], 1000, session, save=True)

    # m = session.query(db.PlayerModel).filter_by(id=5).one()
    # for date_tup in session.query(db.PlayerGameSalary.date).distinct().all():
    #     date = date_tup[0]
    #     predict_slate(date, session, conn, m)
    # solve_predicted_slate(date, session)
    # update_lineup_results(session)

    # Seaborn results:
    # df = pd.read_sql(
    #     session.query(
    #         db.PlayerPrediction.pred,
    #         db.PlayerPrediction.stddev,
    #         db.PlayerGameStats.pts,
    #         db.PlayerGameStats.reb,
    #         db.PlayerGameStats.ast,
    #         db.PlayerGameStats.stl,
    #         db.PlayerGameStats.blk,
    #         db.PlayerGameStats.to,
    #     )
    #     .join(
    #         db.PlayerGameStats,
    #         (db.PlayerPrediction.gid == db.PlayerGameStats.gid)
    #         & (db.PlayerPrediction.date == db.PlayerGameStats.date),
    #     )
    #     .statement,
    #     session.bind,
    # )
    # df["fpts"] = nba.fanduel_points(data=df)
    #
    # df["delta"] = df["fpts"] - df["pred"]
    # df["gt"] = df["pred"] > 45
    # df["sds"] = df["delta"] / df["stddev"]
    # # sns.relplot(x="pred", y="sds", data=df)
    # # sns.distplot(df["sds"])
    # g = sns.FacetGrid(df, col="gt")
    # g.map(sns.distplot, "sds")


if __name__ == "__main__":
    # requests_cache.install_cache("nbapy_cache")
    main()
