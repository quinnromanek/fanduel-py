import pytest
from quinnba.db import *
from sqlalchemy.engine import Engine
from ..factory.schema import NbaStatLineFactory, FanduelPlayerFactory
import pandas as pd

import datetime
import testing.postgresql

SEASON = 2016

Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True)


def teardown_module(module):
    Postgresql.clear_cache()


@pytest.fixture
def conn() -> engine:
    postgres = Postgresql()
    eng = connect(postgres.url())
    yield eng
    eng.dispose()
    postgres.stop()


@pytest.fixture
def games_input():
    return [
        pd.read_csv("testdata/ingestion0.csv", parse_dates=["date"]),
    ]


@pytest.fixture
def games_expect():
    return pd.read_csv(
        "testdata/ingestion0-expected.csv",
        parse_dates=["date"],
    )


@pytest.fixture
def salary_input():
    return pd.read_csv("testdata/ingestion_salaries.csv", parse_dates=["date"])


def test_player_ingestion(conn: Engine, salary_input):
    session = Session()
    ingest_players(salary_input, session)
    assert lookup_gid(203490, "Otto Porter Jr.", session, attr="nbaid") == 1
    assert session.query(Player).filter_by(nbaid=203490).first() is not None
    assert lookup_gid(203490, "Otto Porter Jr.", session, "nbaid") == 1
    assert lookup_gid(2743, "Kris Humphries", session, "nbaid") == 2
    assert lookup_gid(201971, "DeJuan Blair", session, "nbaid") == 201971
    assert lookup_gid(203506, "Victor Oladipo", session, "nbaid") == 6
    assert lookup_gid(203894, "Vuntr Oladipo", session, "nbaid") == 203894

    assert lookup_gid(678, "Otto Porter Jr.", session, "nbaid") == 1
    assert lookup_gid(676, "Otto Porter Jr.", session, attr="nerdid") == 1
    otto = session.query(Player).filter_by(gid=1).first()
    assert otto.nbaid == 678
    assert otto.nerdid == 676


# def test_salary_ingestion(conn: Engine, salary_input):
#     s = Session()
#     ingest_players(salary_input, s)
#     ingest_game_salaries(salary_input, conn)
#
#     d = datetime.date(2015, 10, 28)
#     first = s.query(PlayerGameSalary).filter_by(gid=2, date=d).first()
#     assert first is not None
#     assert first.salary == 300
#     assert first.team == "was"
#
#     assert s.query(PlayerGameSalary).count() == len(salary_input)
#     salary_input["date"] = datetime.date(2016, 10, 29)
#     ingest_game_salaries(salary_input, conn)
#     assert s.query(PlayerGameSalary).count() == len(salary_input) * 2


def test_salary_ingestion_missing_players(conn: Engine):
    session = Session()
    session.add(Game(id=1, date="2020-01-01", home="was", away="atl"))
    session.add(Game(id=2, date="2020-01-01", home="orl", away="tor"))
    session.add(Game(id=3, date="2020-01-02", home="orl", away="tor"))

    session.add(Player(first="tony", last="montana", name="tony montana"))
    session.add(Player(nbaid=22, first="paul", last="blart", name="paul blart"))
    session.commit()
    salaries = [
        FanduelPlayerFactory.build(
            fanduel_id=11,
            player_name="tony montana",
            date="2020-01-01",
            team="orl",
            injury_status="GTD",
        ),
        FanduelPlayerFactory.build(
            fanduel_id=22, player_name="paul blart", date="2020-01-01", team="was"
        ),
        FanduelPlayerFactory.build(
            fanduel_id=33,
            player_name="howard hunt",
            date="2020-01-01",
            team="tor",
        ),
    ]

    ingest_game_salaries(pd.DataFrame(s.kwargs for s in salaries), session)
    ts = session.query(Player).filter(Player.first == "tony").one()
    ps = session.query(Player).filter(Player.first == "paul").one()
    hs = session.query(Player).filter(Player.first == "howard").one()

    assert session.query(PlayerGameSalary).count() == 3
    tony = session.query(PlayerGameSalary).filter(PlayerGameSalary.gid == ts.gid).one()
    assert tony is not None
    assert ts.gid == 1
    assert ts.fanduel_id == 11

    assert tony.game_id == 2
    assert not tony.active
    assert tony.injury_status == "GTD"
    paul = session.query(PlayerGameSalary).filter(PlayerGameSalary.gid == ps.gid).one()
    assert paul is not None
    assert paul.game_id == 1
    assert paul.active

    assert (
        session.query(PlayerGameSalary)
        .filter(PlayerGameSalary.gid == hs.gid)
        .one()
        .game_id
        == 2
    )


def test_game_ingestion(conn, salary_input, games_input, games_expect):
    s = Session()
    ingest_players(salary_input, s)
    ingest_game_salaries(salary_input, conn)

    ingest_game_results(games_input, conn, s)

    loaded = pd.read_sql(
        s.query(PlayerGameStats).statement, s.bind, parse_dates=["date"]
    )
    pd.testing.assert_frame_equal(
        loaded,
        games_expect,
        check_column_type=False,
        check_dtype=False,
        check_like=True,
    )

    assert s.query(TeamGameStats).count() == 2
    team = s.query(TeamGameStats).filter_by(team="orl").first()
    assert team is not None
    assert team.fgm == 12
    assert team.date == datetime.date(2015, 10, 28)
    assert team.season == 2016
    assert team.opp == "was"

    player = s.query(Player).filter_by(gid=7).first()
    assert player.nbaid is None

    missing = s.query(MissingPlayer).filter_by(nbaid=203894).one()
    assert missing.first == "vuntr"

    fix_gid(203894, 7, s)

    player = s.query(Player).filter_by(gid=7).first()
    assert player.nbaid == 203894
    assert s.query(PlayerGameStats).filter_by(gid=203894).first() is None
    found = s.query(PlayerGameStats).filter_by(gid=7).first()
    assert found is not None
    assert found.pos == "C"

    assert s.query(MissingPlayer).filter_by(nbaid=203894).first() is None


def test_game_ingestion_missing_player_creation(conn):
    with Session() as session:
        session.add(Game(id=1))
        session.add(Player(first="tony", last="montana", name="tony montana"))
        session.add(Player(nbaid=22, first="paul", last="blart", name="paul blart"))
        session.commit()
        stats = [
            NbaStatLineFactory.build(
                player_id=11, player_name="tony montana", date="2020-01-01", game_id=1
            ),
            NbaStatLineFactory.build(
                player_id=22, player_name="paul blart", date="2020-01-01", game_id=1
            ),
            NbaStatLineFactory.build(
                player_id=33, player_name="howard hunt", date="2020-01-01", game_id=1
            ),
        ]

        ingest_game_results(pd.DataFrame(s.kwargs for s in stats), session)

        assert session.query(Player).count() == 3
        assert session.query(PlayerGameStats).count() == 3
        missing = session.query(Player).filter(Player.first == "howard").first()
        assert missing is not None
        assert missing.nbaid == 33
        assert missing.missing
        fixed = session.query(Player).filter(Player.first == "tony").first()
        assert fixed.nbaid == 11


def test_game_ingestion_missing_player_creation_no_update(conn):
    with Session() as session:
        session.add(Game(id=1))
        session.add(Player(first="tony", last="montana", name="tony montana"))
        session.commit()
        stats = [
            NbaStatLineFactory.build(
                player_id=11, player_name="tony montana", date="2020-01-01", game_id=1
            ),
            NbaStatLineFactory.build(
                player_id=33, player_name="howard hunt", date="2020-01-01", game_id=1
            ),
        ]

        ingest_game_results(pd.DataFrame(s.kwargs for s in stats), session)

        assert session.query(Player).count() == 2
        assert session.query(PlayerGameStats).count() == 2
        missing = session.query(Player).filter(Player.first == "howard").first()
        assert missing is not None
        assert missing.nbaid == 33
        assert missing.missing
        fixed = session.query(Player).filter(Player.first == "tony").first()
        assert fixed.nbaid == 11


def test_lineup_entries(conn, salary_input):
    s = Session()
    ingest_players(salary_input, s)

    c = Contest()
    s.add(c)

    s.add(
        Lineup(
            date="2015-10-28",
            season=2016,
            mid=1,
            contest=c,
            expected=10,
            stddev=20,
            entries=[LineupEntry(gid=2, pos="PG"), LineupEntry(gid=7, pos="SG")],
        )
    )

    s.commit()

    entries = s.query(LineupEntry).all()
    assert 2 == len(entries)
    assert entries[0].lineup_id is not None


def test_contest(conn):
    s = Session()
    c = Contest(
        name="Test",
        entries=10,
        cost=4,
        tiers=[
            (1, 1000),
            (0.6, 500),
        ],
        days=[
            ContestDay(day=1),
            ContestDay(day=3),
        ],
    )

    assert [500, 1000] == c.payouts
    assert 0.6 == c.tiers[0][0]

    s.add(c)
    s.commit()

    tues = contests_for_day(datetime.date(2021, 1, 19), s)
    assert 1 == len(tues)
    assert tues[0].id == c.id
    mon = contests_for_day(datetime.date(2021, 1, 18), s)
    assert 0 == len(mon)
