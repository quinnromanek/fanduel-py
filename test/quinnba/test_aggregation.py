import pytest

from quinnba import model
from quinnba.aggregation import *
from quinnba.db import (
    connect,
    PlayerGameStats,
    PlayerAverageStats,
    TeamAverageStats,
    TeamAverageOppStats,
    PlayerSituationalStats,
)
import testing.postgresql
import pandas as pd
import datetime

from quinnba.nba import fanduel_points
from sqlalchemy.engine import Engine
from typing import Generator

SEASON = 2016
DATE = datetime.date(2015, 11, 15)

Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True)


def teardown_module(_):
    Postgresql.clear_cache()


@pytest.fixture
def conn() -> Generator[Engine]:
    postgres = Postgresql()
    eng = connect(postgres.url())
    yield eng
    eng.dispose()
    postgres.stop()


@pytest.fixture
def slate() -> pd.DataFrame:
    return pd.DataFrame(
        {
            "pos": ["PG", "SG", "C", "SF", "PF"],
            "salary": [200, 150, 300, 400, 500],
            "date": pd.Series([DATE, DATE, DATE, DATE, DATE], dtype="datetime64[ns]"),
            "gid": [1, 5, 15, 20, 21],
            "team": ["was", "orl", "orl", "mia", "nyk"],
            "opp": ["orl", "was", "was", "lal", "bkn"],
            "active": [True, True, True, True, False],
        }
    )


@pytest.fixture
def full_slate() -> pd.DataFrame:
    return (
        pd.read_csv("testdata/aggregation-in-slate.csv", parse_dates=["date"])
        .sort_values(["date"])
        .set_index(["gid", "date"])
    )


@pytest.fixture
def player_stats() -> pd.DataFrame:
    return pd.read_csv("testdata/aggregation-in.csv", parse_dates=["date"]).set_index(
        ["gid", "date"]
    )


@pytest.fixture
def team_stats() -> pd.DataFrame:
    return pd.read_csv(
        "testdata/aggregation-in-team.csv", parse_dates=["date"]
    ).set_index(["team", "date"])


@pytest.fixture
def player_expected() -> pd.DataFrame:
    return pd.read_csv("testdata/aggregation-players.csv", parse_dates=["date"])


def test_aggregate_players(conn, slate, player_stats, player_expected):
    player_stats.to_sql(
        PlayerGameStats.__tablename__, conn, if_exists="append", index=True
    )
    s = Session()
    aggregate_players_before_date(slate, conn, s)

    results = pd.read_sql(
        s.query(PlayerAverageStats).statement, s.bind, parse_dates=["date"]
    )

    pd.testing.assert_frame_equal(
        results,
        player_expected,
        check_dtype=False,
        check_like=True,
        check_column_type=False,
    )

    assert s.query(PlayerAverageStats).filter_by(gid=15, date=DATE).first() is None

    situational = s.query(PlayerSituationalStats).filter_by(gid=1).first()
    assert situational is not None
    assert situational.recent_games == [True, False, False, True, False]


def test_aggregate_teams(conn, slate, team_stats):
    team_stats.to_sql(TeamGameStats.__tablename__, conn, if_exists="append", index=True)

    s = Session()
    aggregate_teams_before_date(slate, conn, s)
    was = s.query(TeamAverageStats).filter_by(date=DATE, team="was").first()
    assert was is not None
    assert was.fgm == 9
    orl = s.query(TeamAverageStats).filter_by(date=DATE, team="orl").first()
    assert orl is not None
    assert orl.fgm == 6

    aggregate_opponents_before_date(slate, conn, s)

    was_opp = s.query(TeamAverageOppStats).filter_by(date=DATE, team="was").first()
    assert was_opp is not None
    assert was_opp.fgm == 6
    orl_opp = s.query(TeamAverageOppStats).filter_by(date=DATE, team="orl").first()
    assert orl_opp is not None
    assert orl_opp.fgm == 9


def test_model_input(conn, slate, team_stats, player_stats):
    player_stats.to_sql(
        PlayerGameStats.__tablename__, conn, if_exists="append", index=True
    )
    team_stats.to_sql(TeamGameStats.__tablename__, conn, if_exists="append", index=True)
    s = Session()

    aggregate_players_before_date(slate, conn, s)
    aggregate_teams_before_date(slate, conn, s)
    aggregate_opponents_before_date(slate, conn, s)

    model_input = load_slate_model_input(slate, s)
    expected = pd.Index(model.FEATURES)

    assert len(model_input) == 2

    assert expected.isin(
        model_input.columns
    ).all(), f"Indices don't match, missing: {set(expected) - set(model_input.columns)}"


def test_training_input(conn, full_slate, team_stats, player_stats):
    full_slate.to_sql(
        PlayerGameSalary.__tablename__, conn, if_exists="append", index=True
    )
    player_stats.to_sql(
        PlayerGameStats.__tablename__, conn, if_exists="append", index=True
    )
    team_stats.to_sql(TeamGameStats.__tablename__, conn, if_exists="append", index=True)

    s = Session()

    for d, slate in full_slate.reset_index().groupby("date"):
        aggregate_players_before_date(slate, conn, s)
        aggregate_teams_before_date(slate, conn, s)
        aggregate_opponents_before_date(slate, conn, s)

    model_input = load_training_input([SEASON], fanduel_points, s)
    expected = pd.Index(
        [
            "fpts",
        ]
        + model.FEATURES
    )

    assert 8 == len(model_input)

    assert expected.isin(
        model_input.columns
    ).all(), f"Indices don't match, missing: {set(expected) - set(model_input.columns)}"

    model.extract_model_data(model_input)
    # Uncomment to generate input testdata for test_model
    # model_input.to_csv("training-in.csv")


def test_load_slate(conn, slate):
    slate.set_index("gid", "date").to_sql(
        PlayerGameSalary.__tablename__, conn, if_exists="append", index=True
    )

    s = Session()

    assert 5 == s.query(PlayerGameSalary).count()

    loaded = load_slate(DATE, s)
    assert 4 == len(loaded)
