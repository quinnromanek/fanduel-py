from unittest.mock import patch

import pytest
import pandas as pd

from quinnba.db import connect, Session, PlayerPrediction, PlayerModel
from quinnba.model import (
    train_new_model,
    most_recent_model,
    FEATURES,
    predict,
    MODEL_EQUATION,
    store_predictions,
)

import testing.postgresql

Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True)


def teardown_module(_):
    Postgresql.clear_cache()


@pytest.fixture
def conn():
    postgres = Postgresql()
    eng = connect(postgres.url())
    yield eng
    eng.dispose()
    postgres.stop()


@pytest.fixture
def training_input() -> pd.DataFrame:
    return pd.read_csv("testdata/training-in.csv", parse_dates=["date"])


@pytest.fixture
def slate_input() -> pd.DataFrame:
    return pd.read_csv("testdata/training-slate.csv", parse_dates=["date"])


@patch("quinnba.model.load_training_input")
def test_train_new_model(mock_input, training_input, slate_input, conn):
    s = Session()

    mock_input.return_value = training_input

    assert most_recent_model(s) is None

    train_new_model([2016], 10, s)
    recent_model = most_recent_model(s)
    assert recent_model is None

    m = s.query(PlayerModel).first()
    m.active = True
    s.commit()

    recent_model = most_recent_model(s)
    assert recent_model is not None

    assert str(hash(tuple([2016]))) == recent_model.seasons_hash
    assert str(hash(tuple(FEATURES))) == recent_model.features_hash
    assert 10 == recent_model.samples
    assert recent_model.equation == MODEL_EQUATION
    assert 10 == len(recent_model.model)

    predictions = predict(recent_model, slate_input)
    assert "stddev" in predictions
    assert "pred" in predictions
    assert "samples" in predictions
    assert "var" in predictions

    store_predictions(predictions, conn)

    pred = s.query(PlayerPrediction).filter_by(gid=1).first()
    assert pred is not None
    assert pred.season == 2016
    assert pred.mid == recent_model.id
