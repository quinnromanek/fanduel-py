import pytest
import testing.postgresql
from quinnba.db import connect, Player, PlayerGameStats, PlayerGameSalary
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session
from graphene.test import Client
from quinnba.server.schema import schema

Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True)


def teardown_module(_):
    Postgresql.clear_cache()


@pytest.fixture
def conn():
    postgres = Postgresql()
    eng = connect(postgres.url())
    yield eng
    eng.dispose()
    postgres.stop()


@pytest.fixture
def client() -> Client:
    return Client(schema)


class TestPromoteMissingPlayer:
    def test_mutate(self, conn: Engine, client: Client):
        session = Session(conn)
        with session.begin():
            session.add(Player(gid=3, first="tony", last="montana", missing=True))
            session.add(Player(gid=5, first="brandon", last="enroth", missing=True))

        context = {"session": Session(conn)}
        results = client.execute(
            """
            mutation {
                promoteMissingPlayer(playerIds: [3]) {
                    players {
                        gid
                    }
                }
            }
        """,
            context=context,
        )

        assert not session.query(Player).filter(Player.gid == 3).one().missing
        assert session.query(Player).filter(Player.gid == 5).one().missing


class TestCorrectMissingPlayer:
    def test_nbaid(self, conn: Engine, client: Client):
        with Session(conn) as session:
            with session.begin():
                session.add(
                    Player(gid=3, nbaid=10, first="tony", last="montana", missing=True)
                )
                session.add(
                    Player(
                        gid=5,
                        first="tonith",
                        last="montana",
                        missing=False,
                        fanduel_id=3,
                    )
                )

        with Session(conn) as s:
            context = {"session": s}
            client.execute(
                """
                mutation {
                    correctMissingPlayer(targetId: 5, sourceId: 3, field: nbaid) {
                        player {
                            gid
                        }
                    }
                }
            """,
                context=context,
            )

        with Session(conn) as session:
            assert session.query(Player).filter(Player.gid == 3).one_or_none() is None
            assert not session.query(Player).filter(Player.gid == 5).one().missing
            assert session.query(Player).filter(Player.gid == 5).one().nbaid == 10
