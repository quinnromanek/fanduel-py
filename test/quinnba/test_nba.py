from quinnba import nba
from datetime import date


def test_into_season():
    assert nba.into_season(2016) == "2015-16"
    assert nba.into_season(2020) == "2019-20"


def test_current_date():
    assert nba.current_season(date(2016, 1, 1)) == 2016
    assert nba.current_season(date(2015, 10, 1)) == 2016
    assert nba.current_season(date(2015, 6, 1)) == 2015
