import quinnba.server.schema.stats as stats
from quinnba.server.schema.mutations import FanduelPlayer
import factory
import quinnba.nba as nba


class NbaStatLineFactory(factory.Factory):
    class Meta:
        model = stats.NbaStatLine

    player_id = factory.Faker("random_int")
    player_name = factory.Faker("name")

    game_id = factory.Faker("random_int")

    # Player model
    date = factory.Faker("date")
    team = factory.Faker("random_element", elements=nba.Team.__members__.keys())
    opp = factory.Faker("random_element", elements=nba.Team.__members__.keys())
    # season
    # pos
    start = True
    inj = False
    out = False

    # TraditionalStatsMixin model
    min = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    fgm = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    fga = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    fg3m = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    fg3a = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    ftm = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    fta = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    oreb = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    dreb = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    reb = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    ast = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    stl = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    blk = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    to = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pf = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pts = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)

    # UsageStatsMixin model
    pct_stl = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_blka = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_ftm = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_reb = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_pts = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_fg3a = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_pfd = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_fta = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_blk = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_fga = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    usg_pct = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_tov = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_fg3m = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_pf = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_dreb = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_fgm = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_ast = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)
    pct_oreb = factory.Faker("pyfloat", min_value=0.0, max_value=20.0)


class FanduelPlayerFactory(factory.Factory):
    class Meta:
        model = FanduelPlayer

    fanduel_id = factory.Faker("random_int")
    player_name = factory.Faker("name")
    pos = factory.Faker("random_element", elements=nba.Position.__members__.keys())
    team = factory.Faker("random_element", elements=nba.Team.__members__.keys())
    opp = factory.Faker("random_element", elements=nba.Team.__members__.keys())
    secondary_pos = factory.Faker(
        "random_element", elements=nba.Position.__members__.keys()
    )
    salary = factory.Faker("random_int")
    injury_status = None
