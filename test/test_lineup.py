import pytest
from pandas import DataFrame
from lineup import position_array, night_score


# @pytest.fixture
# def df():
#     data = {
#         'FD pos':  [4, 4, 4, 5],
#         'Sal':  [200, 150, 50, 20],
#         'FD Sal':  [20000, 15000, 5000, 2000],
#         'GID':  [1, 2, 3, 4],
#         'FDP': [30.0, 25.0, 15.0, 20.0],
#     }
#     return DataFrame(data)
#
#
# def test_position_array(df):
#     arr = position_array(df, 4, 'FDP')
#     assert 1 in arr[-1][-1].players
#     assert 2 in arr[-1][-1].players
#     assert 3 not in arr[-1][-1].players
#     assert arr[-1][-1].value == 55.0
#
#     assert 2 in arr[-1][200].players
#     assert 3 in arr[-1][200].players
#     assert 1 not in arr[-1][200].players
#     assert arr[-1][200].value == 40.0
#
#
# def test_position_array_start_array(df):
#     c_arr = position_array(df, 5, 'FDP')
#     arr = position_array(df, 4, 'FDP', c_arr[-1])
#     assert 1 in arr[-1][-1].players
#     assert 2 in arr[-1][-1].players
#     assert 4 in arr[-1][-1].players
#     assert 3 not in arr[-1][-1].players
#
#     assert 2 in arr[-1][200].players
#     assert 4 in arr[-1][200].players
#     assert 1 not in arr[-1][200].players
#     assert 3 not in arr[-1][200].players
#
#
# def test_night_score(df):
#     value, players = night_score(df, 'FDP')
#     assert [1, 2, 4] == players
#     assert 3 not in players
#     assert value == 75.0


