import datetime as dt

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from quinnba.db import Session, PlayerModel, Contest
from quinnba.cron import solve_predicted_slate
from quinnba.model import most_recent_model

pg = PostgresHook(postgres_conn_id="quinnba_db")
Session.configure(bind=pg.get_sqlalchemy_engine())
eng = pg.get_sqlalchemy_engine()
session = Session()

default_args = {
    "owner": "quinnba",
    "start_date": dt.datetime(2020, 12, 22),
    "retries": 1,
    "retry_delay": dt.timedelta(minutes=5),
}


def solve_lineup(execution_date, dag_run):
    if "model_id" in dag_run.conf:
        model = (
            session.query(PlayerModel).filter_by(id=dag_run.conf["model_id"]).first()
        )
    else:
        model = most_recent_model(session)

    assert "contest_id" in dag_run.conf
    contest = session.query(Contest).filter_by(id=dag_run.conf["contest_id"]).first()
    solve_predicted_slate(
        execution_date.date(), session, check=False, model=model, contests=[contest]
    )


with DAG(
    "create_lineup",
    default_args=default_args,
    schedule_interval=None,
    catchup=False,
) as create_lineup:
    create_task = PythonOperator(task_id="solve_lineup", python_callable=solve_lineup)
