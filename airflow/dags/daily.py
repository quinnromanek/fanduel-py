import datetime as dt

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook

from quinnba.db import Session, Player
import quinnba.cron as cron
from quinnba.downloader import rotoguru_results, rotoguru_preview

pg = PostgresHook(postgres_conn_id="quinnba_db")
Session.configure(bind=pg.get_sqlalchemy_engine())
eng = pg.get_sqlalchemy_engine()
session = Session()

default_args = {
    "owner": "quinnba",
    "start_date": dt.datetime(2020, 12, 22),
    "retries": 1,
    "retry_delay": dt.timedelta(minutes=5),
}


def parse_date(ds: str):
    return dt.datetime.strptime(ds, "%Y-%m-%d").date()


def pull_yesterdays_salaries(yesterday_ds):
    """
    Pulls yesterday's fanduel salaries
    :return:
    """
    current_date = parse_date(yesterday_ds)
    results = rotoguru_results(current_date)
    if len(results) == 0:
        print(f"No results available for {yesterday_ds}")
    cron.ingest_slate(results, current_date, eng, session)


def pull_preview_salaries():
    """
    Pulls today's unplayed salaries
    :return:
    """
    # We only can rely on these being good day-of, so get the real today always.
    date = dt.date.today()
    results = rotoguru_preview(date, session)
    if len(results) == 0:
        print(f"No results available for {date.strftime('%Y-%m-%d')}")
    cron.ingest_slate(results, date, eng, session)


def pull_results(yesterday_ds):
    current_date = parse_date(yesterday_ds)
    cron.pull_latest_results(current_date, eng, session)


def predict_date(yesterday_ds):
    current_date = parse_date(yesterday_ds)
    cron.predict_slate(current_date, session, eng)


def predict_preview_date(ds):
    current_date = parse_date(ds)
    cron.predict_slate(current_date, session, eng)


def solve_yesterdays_lineup(yesterday_ds: str):
    cron.solve_predicted_slate(parse_date(yesterday_ds), session)


def solve_preview_lineup(ds: str):
    cron.solve_predicted_slate(parse_date(ds), session)


def update_old_lineups():
    cron.update_lineup_results(session)


with DAG(
    "slate_update",
    default_args=default_args,
    schedule_interval="0 14 * * * *",
    catchup=False,
) as slate_update:
    slate_task = PythonOperator(
        task_id="pull_salaries",
        python_callable=pull_yesterdays_salaries,
    )

    results_task = PythonOperator(
        task_id="pull_results",
        python_callable=pull_results,
    )

    predict_task = PythonOperator(
        task_id="predict_date",
        python_callable=predict_date,
    )

    solve_task = PythonOperator(
        task_id="solve_lineup",
        python_callable=solve_yesterdays_lineup,
        execution_timeout=dt.timedelta(hours=4),
    )

    preview_task = PythonOperator(
        task_id="pull_preview_salaries",
        python_callable=pull_preview_salaries,
    )

    preview_solve_task = PythonOperator(
        task_id="solve_preview_lineup",
        python_callable=solve_preview_lineup,
    )

    preview_predict_task = PythonOperator(
        task_id="predict_preview_lineup",
        python_callable=predict_preview_date,
    )

    lineup_actual_task = PythonOperator(
        task_id="lineup_actual",
        python_callable=update_old_lineups,
    )

    slate_task >> [results_task, predict_task]
    predict_task >> solve_task
    # Don't have these conflict
    predict_task >> preview_task
    preview_task >> preview_predict_task
    preview_predict_task >> preview_solve_task
    [solve_task, results_task] >> lineup_actual_task
    # Just in case the results are already here
    preview_solve_task >> lineup_actual_task
