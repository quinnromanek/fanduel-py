import datetime
import pandas
import lineup
from lineup import min_sum
from quinnba.downloader import POS_LIST, scrape_csv
import feature
from os import mkdir, listdir
from os.path import isfile, join


KEPT_FIELDS = [
    'First Last',
    'GID',
    'FD Sal',
    'active',
    'FD pos',
    'FPPG',
]

LINEUP_FIELDS = ['First Last', 'Prediction', 'FD pos', 'FD Sal']

def load_results(date):
    return pandas.read_csv('data/2020/results/{}.csv'.format(date.strftime("%Y%m%d")))


class ActualFeature:
    KEY = 'FDP'

    def __init__(self, results):
        self._results = results

    def row_value(self, row):
        player_df = self._results[(self._results['GID'] == row['GID'])]
        try:
            return player_df.iloc[0]['FDP']
        except IndexError:
            raise KeyError("Couldn't find player in results with GID {} {}".format(row['GID'], row['First Last']))


def load_slate(date):
    slate = pandas.read_csv('data/2019/predictions/{}.csv'.format(date.strftime("%Y%m%d")))
    slate['First Last'] = slate['Nickname']
    slate['FD Sal'] = slate['Salary']
    slate['active'] = [1 if s else 0 for s in slate['Injury Indicator'].isnull()]
    slate['FD pos'] = [POS_LIST.index(p) + 1 for p in slate['Position']]
    feature.add_feature(slate, feature.GIDFeature())
    slate = slate[KEPT_FIELDS]
    return slate


def load_lineups(date):
    date_str = date.strftime('%Y%m%d')
    path = 'data/2020/lineups/{}'.format(date_str)
    try:
        lineup_files = [f for f in listdir(path) if isfile(join(path, f))]
        lineups = {}
        gid_feature = feature.GIDFeature()
        for f in lineup_files:
            df = pandas.read_csv(join(path, f))
            feature.add_feature(df, gid_feature)
            lineups[f] = df
    except FileNotFoundError:
        # No lineups
        return {}

    return lineups


def process_slate(date):
    slate = load_slate(date)
    active_slate = slate[(slate['active'] == 1)]

    # Min Method
    _, ids = lineup.night_score(active_slate, 'FPPG', storage_type=lineup.MinPositionEntry)
    _, min_players = lineup.select_lineup(ids, active_slate, key='FPPG')
    min_players['Prediction'] = min_players['FPPG']
    print(ids)

    # Best Total Method
    _, ids = lineup.night_score(active_slate, 'FPPG')
    _, highest_players = lineup.select_lineup(ids, active_slate, key='FPPG')
    highest_players['Prediction'] = highest_players['FPPG']
    date_str = date.strftime('%Y%m%d')
    try:
        mkdir('data/2019/lineups/{}'.format(date_str))
    except FileExistsError:
        pass
    min_players[LINEUP_FIELDS].to_csv('data/2019/lineups/{}/min.csv'.format(date_str))
    highest_players[LINEUP_FIELDS].to_csv('data/2019/lineups/{}/highest_total.csv'.format(date_str))


def daily_update():
    today = datetime.datetime.now()
    yesterday = datetime.datetime.now() - datetime.timedelta(1)
    print("Downloading Yesterday's results...")
    try:
        scrape_csv(yesterday)
    except ValueError:
        print("Couldn't load yesterday's data.")

    lineups = load_lineups(yesterday)
    results = load_results(yesterday)
    results_feature = ActualFeature(results)
    _, ids = lineup.night_score(results, 'FDP')
    _, best_players = lineup.select_lineup(ids, results)
    for name, line in lineups.items():
        feature.add_feature(line, results_feature)
        line['Diff'] = line['FDP'] - line['Prediction']
        print("Lineup: {}, {}".format(name, min_sum(line['FDP'])))
        print(line[['First Last', 'Prediction', 'FDP', 'Diff']])
    print("Best: {} (Total {})".format(min_sum(best_players['FDP']), sum(best_players['FDP'])))
    print(best_players[['First Last', 'FDP']])
    print("Preparing Today's Lineups...")
    process_slate(today)
    print("Lineups Saved.")


if __name__ == "__main__":
    daily_update()
