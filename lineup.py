import copy
import time
from operator import itemgetter
import numpy as np
from numba import jit
import pandas as pd
from typing import List

SAVED_FIELDS = [
    "GID",
    "First Last",
    "Date",
    "H/A",
    "Start",
    "Minutes",
    "active",
    "FD Sal",
    "FD pos",
    "FDP",
    "Opp",
    "Team",
]

POSITION_COUNT = {1: 2, 2: 2, 3: 2, 4: 2, 5: 1}

ARR_POSITION_COUNT = np.array([0, 2, 2, 2, 2, 1])

TOTAL_SALARY = 600
POSITIONS = [5, 4, 3, 2, 1]


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if "log_time" in kw:
            name = kw.get("log_name", method.__name__.upper())
            kw["log_time"][name] = int((te - ts) * 1000)
        else:
            print("%r  %2.2f ms" % (method.__name__, (te - ts) * 1000))
        return result

    return timed


class PositionEntry:
    def __init__(self):
        self.value = 0
        self.players = []

    def __repr__(self):
        return "PositionEntry<{} {}>".format(self.value, self.players)

    def current_value(self):
        return self.value

    def add_player(self, player, player_value):
        new_entry = PositionEntry()
        new_entry.value = self.value + player_value
        new_entry.players = self.players + [player]
        return new_entry

    def potential_value(self, player_value):
        return self.value + player_value


class MinPositionEntry:
    def __init__(self):
        self.value = 0
        self.players = []
        self.min_player = 1000

    def __repr__(self):
        return "MinPositionEntry<{} {}>".format(self.value, self.players)

    def current_value(self):
        if len(self.players) < 2:
            return self.value
        else:
            return self.value - self.min_player

    def add_player(self, player, player_value):
        new_entry = MinPositionEntry()
        new_entry.value = self.value + player_value
        new_entry.players = self.players + [player]
        if self.min_player > player_value:
            new_entry.min_player = player_value
        return new_entry

    def potential_value(self, player_value):
        if player_value < self.min_player:
            return self.value
        else:
            return self.value - self.min_player + player_value


def top_for_salary(night_df, pos, col):
    count_for_pos = POSITION_COUNT[pos]
    salary_map = {}

    for _, row in night_df.iterrows():
        sal = int(row["Sal"])
        if sal not in salary_map:
            salary_map[sal] = [(row["GID"], row[col])]
        elif len(salary_map(sal)) < count_for_pos:
            salary_map[sal].append((row["GID"], row[col]))
            salary_map[sal].sort(key=itemgetter(1))
        else:
            if salary_map[sal][0][1] < row[col]:
                salary_map[sal][1] = (row["GID"], row[col])
            salary_map[sal].sort(key=itemgetter(1))


@jit
def position_array(players, pos, start_array, start_players):
    num_positions = ARR_POSITION_COUNT[pos] + 1
    read_flat = np.copy(start_array)
    for _ in range(num_positions - 1):
        read_flat = np.concatenate((read_flat, start_array))
    read = read_flat.reshape((num_positions, TOTAL_SALARY + 1))
    read_players_flat = np.copy(start_players)
    for _ in range(num_positions - 1):
        read_players_flat = np.concatenate((read_players_flat, start_players))
    read_players = read_players_flat.reshape((num_positions, TOTAL_SALARY + 1, 9))

    write = np.copy(read)
    write_players = np.copy(read_players)

    for player_i in range(len(players)):

        sal_f, player_id, player_value = players[player_i]
        sal = int(sal_f)
        player_id = int(player_id)
        for i in range(1, num_positions):
            write[i, 0:sal] = read[i, 0:sal]
            write_players[i, 0:sal] = read_players[i, 0:sal]
            for j in range(sal, TOTAL_SALARY + 1):
                taken_value = read[i - 1, j - sal]
                if sal <= j and read[i, j] < (taken_value + player_value):
                    # Take the deducted item
                    write_players[i, j] = np.copy(read_players[i - 1, j - sal])
                    write_players[i, j, ((pos - 1) * 2) + (i - 1)] = player_id
                    write[i, j] = taken_value + player_value
                else:
                    # Take the same item from the other array
                    write[i, j] = read[i, j]
                    write_players[i, j] = np.copy(read_players[i, j])

        write, read = read, write
        write_players, read_players = read_players, write_players

    return read[-1], read_players[-1]


MAPPING = {
    "PG": 1,
    "SG": 2,
    "SF": 3,
    "PF": 4,
    "C": 5,
}


def _map_pos(pos):
    if type(pos) != str:
        return pos
    return MAPPING[pos]


def night_score(
    night_df: pd.DataFrame,
    col: str,
    pos_col: str = "FD pos",
    sal_col: str = "FD Sal",
    gid_col="GID",
) -> (int, List[int]):
    """Calculates the optimal lineup for the given night.

    :param night_df: DataFrame containing the players and their salaries.
    :param col: Name of column in night_df containing the score to be optimized.
    :return: (score, list of gids of selected players)
    """
    night_df = night_df.sort_values(col, ascending=False)
    night_df = night_df.dropna()
    night_df["position"] = night_df[pos_col].map(_map_pos)
    night_df["Sal"] = night_df[sal_col].map(lambda s: s / 100)
    last_start = np.zeros(TOTAL_SALARY + 1)
    last_players = np.zeros((TOTAL_SALARY + 1, 9))
    for pos in POSITIONS:
        pos_night_df = night_df[(night_df["position"] == pos)]
        players = np.array(
            [
                [int(row["Sal"]), int(row[gid_col]), row[col]]
                for _, row in pos_night_df.iterrows()
            ]
        )
        last_start, last_players = position_array(
            players, pos, last_start, last_players
        )
    out_players = []
    for p in last_players[-1]:
        if p > 0:
            out_players.append(int(p))

    value, _ = select_lineup(out_players, night_df, col, pos=pos_col, gid=gid_col)
    return last_start[-1], sorted(out_players)


def select_lineup(gids, night_df, key="FDP", pos="FD pos", gid="GID"):
    df = night_df[(night_df[gid].isin(gids))].sort_values(pos)
    return df[key].sum(), df


def min_sum(elements):
    return sum(elements) - min(elements)
