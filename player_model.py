from statsmodels.tsa.arima_model import ARIMA, ARIMAResults
from statistics import mean, stdev
import numpy as np
import os
import json
import re


def load_models(gids, all_data, night_date):
    model_data = all_data[(all_data['Date'] < night_date)]
    player_models = {}
    for gid in gids:
        try:
            model = ARIMAResults.load("models/{}/{}.mod".format(str(night_date)[:10], gid))
            player_models[gid] = PlayerModel(gid, model_data, model)
        except ValueError as err:
            player_models[gid] = PlayerModel(gid, model_data)
        except FileNotFoundError:
            player_models[gid] = PlayerModel(gid, model_data)
            if player_models[gid].model:
                try:
                    os.mkdir("models/{}".format(str(night_date)[:10]))
                except FileExistsError:
                    pass
                player_models[gid].model.save("models/{}/{}.mod".format(str(night_date)[:10], gid))
    return player_models


class PlayerModel:
    def __init__(self, gid, all_data, model=None):
        self.gid = gid
        all_data = all_data.loc[all_data['GID'] == self.gid]
        all_data.sort_values('Date')
        self._data = all_data

        self._observations = all_data['FDP'].values
        self._model = model
        if not self._model:
            self.compute_model()

    @property
    def model(self):
        return self._model

    @property
    def player_data(self):
        return self._data

    def compute_model(self):
        if len(self._observations) > 2 and sum(self._observations) > 0.0:
            # arr = np.array(self._data['OppAvg'].values)
            # arr = np.column_stack((arr,))
            # print(self._observations.shape)
            # print(arr.shape)
            # print(self._observations)
            # print(arr)
            # self._model = ARIMA(self._observations, order=(0, 1, 1), exog=arr).fit(disp=0)
            try:
                self._model = ARIMA(self._observations, order=(0, 1, 1)).fit(disp=0)
            except ValueError as err:
                self._model = None
        else:
            self._model = None

    def forecast(self, row):
        if row['active'] == 0 or len(self._observations) == 0:
            return [0.0], [0.0], [[0.0, 0.0]]
        if self._model:
            return self._model.forecast()
        else:
            avg = mean(self._observations)
            std = stdev(self._observations) if len(self._observations) > 1 else 0.0
            return [avg], [std], [[avg - std, avg + std]]

    def residuals(self):
        return self._model.resid


def all_gids():
    gid_map = {'GID': {}, 'Names':{}}
    try:
        with open('data/gid.json', 'r') as file:
            gid_map = json.load(file)
    except FileNotFoundError:
        pass
    return gid_map


def name_clean(name):
    return re.sub(r'\W', '', name).lower()


def save_gids(df):
    gids = all_gids()
    new_gids = df['GID'].unique()
    for gid in new_gids:
        gid = int(gid)
        name = df[(df['GID'] == gid)].iloc[0]['First Last']
        name = name_clean(name)
        if str(gid) not in gids['GID']:
            print("Adding GID entry for {}:{}".format(gid, name))
            gids['GID'][gid] = name
            gids['Names'][name] = gid
        elif name not in gids['Names']:
            print("Adding alternate name for {}:{}".format(gid, name))
            gids['Names'][name] = gid

    with open('data/gid.json', 'w+') as file:
        json.dump(gids, file)

