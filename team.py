import pandas
from collections import defaultdict


class RollingAverage:
    def __init__(self):
        self.value = 0.0
        self.count = 0

    def add(self, value):
        self.value = (self.value * self.count) / (self.count + 1.0) + (value / (self.count + 1.0))
        self.count += 1


def team_averages(all_data):
    try:
        df = pandas.read_csv('data/team_averages.csv', index_col=0, parse_dates=[0])
        return df
    except FileNotFoundError:
        all_data = all_data.sort_values('Date')
        dates = all_data['Date'].unique()
        teams = all_data['Opp'].unique()
        team_dict = defaultdict(list)
        team_continuous_averages = {}
        for team in teams:
            team_continuous_averages[team] = RollingAverage()
        league_average = RollingAverage()

        for i, date in enumerate(dates):
            date_entries = all_data[(all_data['Date'] == date)]
            for team in teams:
                if team_continuous_averages[team].count > 0:
                    team_dict[team].append(team_continuous_averages[team].value / league_average.value)
                else:
                    team_dict[team].append(1.0)
                point_sum = date_entries[(date_entries['Opp'] == team)]['FDP'].sum()
                if point_sum > 0.0:
                    team_continuous_averages[team].add(point_sum)
                    league_average.add(point_sum)

        df = pandas.DataFrame(team_dict)
        df.index = dates
        df.to_csv('data/team_averages.csv')
        return df


def add_team_averages(df, team_averages):
    opp_avg = []
    for _, row in df.iterrows():
        team_row = team_averages[row['Date']:row['Date']].iloc[0]
        team_val = team_row[row['Opp']]
        opp_avg.append(team_val)
    df['OppAvg'] = opp_avg
    return df
