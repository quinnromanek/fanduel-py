from collections import defaultdict
from random import gauss
import lineup
from player_model import load_models
import operator

DEFAULT_SIMULATIONS = 100


def predict_model(all_data, night_date):
    training_df = all_data.query("active == 1")
    training_df = training_df[(training_df['FDP'] != 0.0)]
    training_df = training_df.dropna()
    player_gids = training_df['GID'].unique()
    all_models = load_models(player_gids, training_df, night_date)
    night_df = all_data[(all_data['Date'] == night_date)]
    night_df = night_df.dropna()
    found_lineups, max_lineup, f_lineup = simulate_night(night_df, all_models)
    best_lineup = max(found_lineups.items(), key=operator.itemgetter(1))[0]
    p_total, p_lineup = lineup.select_lineup(best_lineup, night_df)
    m_total, m_lineup = lineup.select_lineup(max_lineup, night_df)
    f_total, f_lineup = lineup.select_lineup(f_lineup, night_df)
    return p_total, m_total, f_total


def add_predictions(night_df, models):
    predictions = []
    deviations = []
    for _,row in night_df.iterrows():
        try:
            model = models[row['GID']]
            forecast, std, _ = model.forecast(row)
        except KeyError:

            forecast = [0]
            std = [0]
        predictions.append(forecast[0])
        deviations.append(std[0])
    night_df['Pred'] = predictions
    night_df['Dev'] = deviations
    return night_df


def simulate_night(night_df, models, num_sims=DEFAULT_SIMULATIONS):
    night_df = add_predictions(night_df, models)
    simulations = defaultdict(int)
    max_lineup = None
    max_score = float('-inf')
    for i in range(num_sims):
        # Add a random difference to each player
        night_df['Guess'] = [gauss(row['Pred'], row['Dev']) for _, row in night_df.iterrows()]
        score, gids = lineup.night_score(night_df, 'Guess')
        simulations[tuple(gids)] += 1
        if score > max_score:
            max_lineup = gids
    print("")
    frequency = defaultdict(int)
    for v in simulations.keys():
        for gid in v:
            frequency[gid] += 1

    def frequency_value(lineup):
        return sum(frequency[gid] for gid in lineup)

    frequency_lineup = max(simulations.keys(), key=frequency_value)

    return simulations, max_lineup, frequency_lineup
