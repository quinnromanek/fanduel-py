import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, RepeatedKFold, GridSearchCV
from sklearn.feature_selection import SelectKBest, f_regression, mutual_info_regression
from sklearn.tree import DecisionTreeRegressor
from sklearn.pipeline import Pipeline
from sklearn.linear_model import Ridge, LinearRegression
import patsy

MODELS = {
    "decision_tree": DecisionTreeRegressor(),
    "ridge_regression": Ridge(),
}

MODEL_PARAMS = {
    "decision_tree": {
        "lr__max_depth": [i for i in range(2, 10)],
    },
    "ridge_regression": {"lr__alpha": [0.1 * i for i in range(11)]},
}

EQUATION = "FDP ~ Pts_Avg + Pts_Avg:Pts_Opp + Pts_Opp + "
"Reb_Avg + Reb_Avg:Reb_Opp + Reb_Opp + "
"Ast_Avg + Ast_Avg:Ast_Opp + Ast_Opp + "
"Stl_Avg + Stl_Avg:Stl_Opp + Stl_Opp + "
"Blk_Avg + Blk_Avg:Blk_Opp + Blk_Opp + "
"To_Avg + To_Avg:To_Opp + To_Opp + "
"Delta_Pct + Delta_Pct:Min_Avg + "
"Start + Min_Delta + Min_Delta:Min_Avg +"
"Min_Team_Delta + Min_Team_Delta:Min_Avg"


# Current best: Ride(k = 22, alpha = 1.0), MSE = 86.839
def best_model():
    fs = SelectKBest(score_func=f_regression, k=20)
    model = Ridge(alpha=1.0)
    pipeline = Pipeline([("sel", fs), ("lr", model)])
    return pipeline


def extract_model_data(df: pd.DataFrame):
    y, df = patsy.dmatrices(
        EQUATION,
        df,
        return_type="dataframe",
    )
    y = np.ravel(y.to_numpy())
    X = df.to_numpy()
    return X, y, df


def select_model(df: pd.DataFrame):
    X, y, df = extract_model_data(df)

    fs = SelectKBest(score_func=f_regression)

    all_results = {}
    for name, model in MODELS.items():
        cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=2)
        pipeline = Pipeline([("sel", fs), ("lr", model)])
        grid = {
            "sel__k": [i for i in range(1, X.shape[1] + 1)],
        }
        grid.update(MODEL_PARAMS[name])

        search = GridSearchCV(
            pipeline,
            grid,
            scoring="neg_mean_squared_error",
            cv=cv,
            n_jobs=-1,
            refit=True,
        )
        results = search.fit(X, y)

        all_results[name] = results

    for name, results in all_results.items():
        print("%s Best MAE: %.3f" % (name, results.best_score_))

        print("%s Best Config: %s" % (name, results.best_params_))

        means = results.cv_results_["mean_test_score"]
        params = results.cv_results_["params"]
        for mean, param in zip(means, params):
            print(">%.3f with: %r" % (mean, param))

    for name, results in all_results.items():
        print("%s Best MAE: %.3f" % (name, results.best_score_))

        print("%s Best Config: %s" % (name, results.best_params_))

        sel = results.best_estimator_.named_steps["sel"]

        vals = list(zip(list(df.columns), sel.scores_))
        vals.sort(reverse=True, key=lambda x: x[1])
        for i, val in enumerate(vals):
            print(f"{i}:\t{val[0]}\t{val[1]}")
