"""populate game ids

Revision ID: 52a06b94ad26
Revises: cdf081ab137a
Create Date: 2021-12-26 21:49:50.225446

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "52a06b94ad26"
down_revision = "cdf081ab137a"
branch_labels = None
depends_on = None


def join_games(tablename: str):
    op.execute(
        """
        update {0}
          set game_id = games.id
        from games
        where games.date = {0}.date and (games.home = {0}.team or games.away = {0}.team)
    """.format(
            tablename
        )
    )


def upgrade():
    join_games("teamgamesstats")
    join_games("playergamesstats")


def downgrade():
    op.execute("update teamgamesstats set game_id=null")
    op.execute("update playergamesstats set game_id=null")
