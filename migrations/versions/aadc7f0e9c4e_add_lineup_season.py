"""add Lineup season

Revision ID: aadc7f0e9c4e
Revises: 0c00b2819753
Create Date: 2021-01-01 13:14:08.871605

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'aadc7f0e9c4e'
down_revision = '0c00b2819753'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('lineups', sa.Column('season', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('lineups', 'season')
    # ### end Alembic commands ###
