"""add salary injury status

Revision ID: 90d67d5b6af0
Revises: aec7d04682f1
Create Date: 2022-01-23 21:07:48.121789

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '90d67d5b6af0'
down_revision = 'aec7d04682f1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('playergamessalary', sa.Column('injury_status', sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('playergamessalary', 'injury_status')
    # ### end Alembic commands ###
