import pandas as pd
import matplotlib.pyplot as plt
import lineup
from player_model import PlayerModel
import prediction
from statistics import mean, median
from math import sqrt
from player_model import save_gids
from fanduel import load_results
from collections import defaultdict
import datetime
import model
import math
from sklearn.utils import resample
import stats
from quinnba import nba

import seaborn as sns

sns.set_style("darkgrid")


def load_stats(file="stats.csv", delim=","):
    frame = pd.read_csv(file, delimiter=delim, parse_dates=["Date"])
    return frame[lineup.SAVED_FIELDS + ["OppAvg"]]


def predict_model_on_test():
    df = load_stats("stats_team_avgs.csv")

    # print("Adding team averages")
    # team_df = team.team_averages(df)
    # df = team.add_team_averages(df, team_df)
    # df.to_csv("stats_team_avgs.csv")

    df = df.sort_values("Date")
    all_dates = df["Date"].unique()
    train_size = int(len(all_dates) * 0.66)
    train, test = all_dates[0:train_size], all_dates[train_size : len(df)]
    test_date = test[0]
    test_df = df[(df["Date"] == test_date)]
    print(lineup.night_score(test_df, "FDP"))

    print("Calculating...")
    night_dates = all_dates[10:]
    out_df = pd.DataFrame(
        columns=["Common", "Best", "Frequent", "Optimal", "Players"], index=night_dates
    )
    for i, night_date in enumerate(night_dates):
        print(
            "Running Model for {} ({}/{})".format(
                str(night_date)[:10], i + 1, len(night_dates)
            )
        )
        common, best, frequent = prediction.predict_model(df, night_date)
        night_df = df[(df["Date"] == night_date)]
        optimal, _ = lineup.night_score(night_df, "FDP")
        total_players = len(night_df)
        out_df.loc[night_date] = {
            "Common": common,
            "Best": best,
            "Frequent": frequent,
            "Optimal": optimal,
            "Players": total_players,
        }
        print(common, best, frequent, optimal)
    out_df["Common_diff"] = out_df["Common"] - out_df["Optimal"]
    out_df["Best_diff"] = out_df["Best"] - out_df["Optimal"]
    out_df["Frequent_diff"] = out_df["Frequent"] - out_df["Optimal"]
    out_df.loc["mean"] = df.mean()
    out_df.to_csv("data/model_results.csv")


def plot_resid():
    df = load_stats("stats_team_avgs.csv")
    training_df = df.query("active == 1")
    training_df = training_df[(training_df["FDP"] != 0.0)]
    training_df = training_df.dropna()
    training_df = training_df.sort_values("Date")
    # all_models = load_models(player_gids, training_df)
    #
    # p_model = all_models[3571]

    all_dates = training_df["Date"].unique()
    training_df = training_df[(training_df["Date"] < all_dates[5])]
    p_model = PlayerModel(3571, training_df)
    # opp_vals = p_model.player_data['OppAvg'].values[1:]

    date_vals = p_model.player_data["Date"].values[1:5]
    print("Mean: {}".format(mean(p_model.model.resid)))
    print("Median: {}".format(median(p_model.model.resid)))
    print("RMSE {}".format(sqrt(mean(n ** 2 for n in p_model.model.resid))))
    p_model.model.plot_predict()
    # plt.scatter(x=date_vals, y=p_model.model.resid)
    plt.show()


def create_gid_db():
    yesterday = datetime.datetime.now() - datetime.timedelta(1)
    results = load_results(yesterday)
    save_gids(results)


def fanduel_points(
    pts="pts", reb="reb", ast="ast", blk="blk", stl="stl", to="to", data=None
):
    if data is not None:
        pts = data[pts]
        reb = data[reb]
        ast = data[ast]
        blk = data[blk]
        stl = data[stl]
        to = data[to]
    return pts + reb * 1.2 + ast * 1.5 + blk * 3.0 + stl * 3.0 - to


PLAYERS_STATS_RETAIN = [
    "Start",
    "Min",
    "Pts",
    "Ast",
    "Reb",
    "Blk",
    "Stl",
    "To",
    "Pf",
    "Opp",
    "Team",
    "Role",
    "Win_Pct",
    "Win_Pct_Opp",
] + stats.TEAM_AVG_STATS


def combine_if(role):
    return lambda val, switch: switch if val == role else val


def construct_training_data():
    players_df = stats.NBA_BOX.build()
    teams_avg = stats.NBA_TEAMS_AVG.build()
    players_avg = stats.NBA_BOX_AVG.build()
    teams_box = stats.NBA_TEAMS_BOX.build()
    teams_box = stats.rolling_avg(teams_box, ["Min_Big", "Min_PG", "Min_Wing"], "Team")
    teams_box["Min_Big_Delta"] = teams_box["Min_Big"] - teams_box["Min_Avg_Big_Avail"]
    teams_box["Min_Wing_Delta"] = (
        teams_box["Min_Wing"] - teams_box["Min_Avg_Wing_Avail"]
    )
    teams_box["Min_PG_Delta"] = teams_box["Min_PG"] - teams_box["Min_Avg_PG_Avail"]
    teams_box["Min_Team_Delta"] = (
        teams_box["Min_Wing_Delta"]
        + teams_box["Min_PG_Delta"]
        + teams_box["Min_Big_Delta"]
    )
    # Exclude Playoffs
    players_df = players_df[
        players_df.index.get_level_values("Date") < pd.to_datetime("2016-04-16")
    ]
    players_df = players_df[
        players_df.index.get_level_values("Date") > pd.to_datetime("2015-11-15")
    ]

    players_df["Start"] = (~players_df["Start_Position"].isnull()).astype(int)

    players_df = (
        players_df.reset_index()
        .join(
            teams_avg[stats.TEAM_AVG_STATS + ["Win_Pct", "Win_Pct_Opp"]],
            on=["Opp", "Date"],
        )
        .set_index(["GID", "Date"])[PLAYERS_STATS_RETAIN]
        .dropna()
    )
    players_df = (
        players_df.reset_index()
        .join(
            teams_box[
                ["Min_Big_Delta", "Min_Wing_Delta", "Min_PG_Delta", "Min_Team_Delta"]
            ],
            on=["Team", "Date"],
        )
        .set_index(["GID", "Date"])
        .dropna()
    )

    players_df["Min_Delta"] = (
        players_df["Role"]
        .combine(players_df["Min_Big_Delta"], combine_if("Big"))
        .combine(players_df["Min_Wing_Delta"], combine_if("Wing"))
        .combine(players_df["Min_PG_Delta"], combine_if("PG"))
    )
    players_df["Delta_Pct"] = players_df["Win_Pct_Opp"] - players_df["Win_Pct"]
    players_df = players_df.join(
        players_avg[stats.PLAYER_AVG_STATS], rsuffix="_Avg"
    ).dropna()
    players_df["FDP"] = fanduel_points(
        "Pts", "Reb", "Ast", "Blk", "Stl", "To", data=players_df
    )
    return players_df


def download_data():
    df = stats.FANDUEL_BASE.build()
    dates = df.reset_index()["Date"].unique()
    lookup = stats.PlayerLookup(df)
    date = datetime.date(2018, 2, 14)
    games = nba.pull_games_for_date(pd.Timestamp(date), lookup)
    # for i, game in enumerate(games):
    #     game.to_csv("test/testdata/test{}.csv".format(i))


def analyze():
    # df = stats.FANDUEL_ACTIVE.build()
    # df = df.dropna()
    # # Exclude outliers
    #
    # for r in stats.ROLLING:
    #     print("{}, RMSE: {} MAE: {}".format(r, sqrt(mean_squared_error(df['FDP'], df['FDP_mean{}'.format(r)])),
    #                                         mean_absolute_error(df['FDP'], df['FDP_mean{}'.format(r)])))
    # print("{}, RMSE: {} MAE: {}".format('all', sqrt(mean_squared_error(df['FDP'], df['FDP_mean'])),
    #                                     mean_absolute_error(df['FDP'], df['FDP_mean'])))

    # sns.relplot(x='Delta_Abs', y='Total_Pts', data=team_df, ax=ax2)

    # player_df = stats.NBA_BOX.build()
    # player_df = player_df.join(stats.NBA_BOX_AVG.build()['Min'], rsuffix='_Avg')
    # j_df = player_df.reset_index().pivot_table(index=['GID', 'Date'], columns='Role', values=['Min', 'Min_Avg'])
    # j_df.columns = j_df.columns.map('_'.join)
    # print(j_df.head())
    # player_df.to_csv('testdata/players.csv')

    player_df = construct_training_data()

    model.select_model(player_df)
    return
    fdb = stats.FANDUEL_BASE.build()
    player_df = player_df.join(fdb[["FD Sal", "FD pos"]]).dropna()

    idx = pd.IndexSlice
    night_df = player_df.loc[idx[:, [pd.to_datetime("2015-12-12")]], idx[:]]
    night_df.to_csv("testdata/night.csv")

    score, best = lineup.night_score(night_df.reset_index("GID"), "FDP")
    print(score)
    print(best)

    m = model.best_model()

    freq = defaultdict(int)

    for i in range(10):
        resampled_df = resample(player_df, n_samples=math.floor(sqrt(len(player_df))))
        X, y = model.extract_model_data(resampled_df)
        m.fit(X, y)

        Xp, _ = model.extract_model_data(night_df)
        night_df["Pred"] = m.predict(Xp)
        pscore, pbest = lineup.night_score(night_df.reset_index("GID"), "Pred")

        if i == 9:
            night_df["Resid"] = night_df["FDP"] - night_df["Pred"]
            night_df.to_csv("night_resid.csv")

        print(i)
        print(pscore)
        print(pbest)
        best_str = "-".join(str(v) for v in pbest)
        freq[best_str] += 1

    best = {k: v for k, v in sorted(freq.items(), key=lambda item: -item[1])}
    for item, count in best.items():
        gids = [int(i) for i in item.split("-")]
        score, _ = lineup.select_lineup(gids, night_df.reset_index("GID"))
        print("{}: count {} FDP {}".format(item, count, score))

    sns.scatterplot(data=night_df, x="Resid", y="Min_Team_Delta", hue="Role")
    plt.show()

    # team_df['Win'] = team_df['Pts'] > team_df.reset_index().join(team_df, on=['Opp', 'Date'])


# model.select_model(players_df)


# Model improvements
# - Add binary flag value for games over last X days
# - Tune/Transform role minutes feature
if __name__ == "__main__":
    # predict_model_on_test()
    # plot_resid()
    analyze()
    # download_data()
