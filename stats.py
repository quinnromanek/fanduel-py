import os
import logging
import pandas as pd
import sys
from typing import Optional
import lineup
from sklearn.preprocessing import MinMaxScaler

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)


class Package:
    """
    Package creates a hermetic path for defining and adding features to data
    """

    def __init__(self, base, parse_dates=['Date'], out=None, rules=None, *args, **kwargs):
        self.base = base
        self.out = out
        self.rules = rules or []
        self.args = args
        self.kwargs = kwargs
        self.kwargs['parse_dates'] = parse_dates

    def with_rule(self, cols, name, rule):
        self.rules.append((cols, name, rule))
        return self

    def build(self, rebuild=False) -> pd.DataFrame:
        base_data = None
        if self.out is not None and not rebuild and os.path.exists(self.out) and os.path.isfile(self.out):
            log.info("Building package from existing output %s", self.out)
            if 'usecols' in self.kwargs and 'index_col' in self.kwargs:
                self.kwargs['usecols'] += self.kwargs['index_col']

            base_data = pd.read_csv(self.out, *self.args, **self.kwargs)
            return base_data
        elif isinstance(self.base, Package):
            log.info("Building package from base package %s", self.base.out)
            base_data = self.base.build()
            if 'usecols' in self.kwargs:
                cols = list(set(self.kwargs['usecols']) & set(base_data.columns))
                base_data = self.base.build()[cols]
        elif os.path.exists(self.base) and os.path.isfile(self.base):
            if 'usecols' in self.kwargs and 'index_col' in self.kwargs:
                self.kwargs['usecols'] += self.kwargs['index_col']
            log.info("Building package from base data %s", self.base)
            base_data = pd.read_csv(self.base, *self.args, **self.kwargs)
        else:
            log.fatal("Couldn't find data for base file %s", self.base)
            return

        if self.out is None:
            return base_data

        for cols, name, rule in self.rules:
            if len(cols) > 0 and not any(col not in base_data for col in cols):
                log.info("Cols %s already present", cols)
                continue

            log.info("Applying rule %s", name)
            base_data = rule(base_data)

            if base_data is None:
                log.fatal("Rule %s didn't return a valid DataFrame", name)

            for col in cols:
                if col not in base_data:
                    log.error("Rule specified %s would be present after application but it is missing.", col)

        base_data.to_csv(self.out)
        return base_data


def sorted_rule(col='Date'):
    return [], 'Sorted', lambda df: df.sort_values(col)


def only_played():
    return [], 'Only Played Games', lambda df: df.query("active == 1 & Minutes > 0")


def apply(func, *args, **kwargs):
    return [], '{}'.format(func), lambda df: func(df, *args, **kwargs)


def best_lineups():
    def select_best_lineup(df: pd.DataFrame) -> pd.DataFrame:
        _, gids = lineup.night_score(df, 'FDP')
        return df[df['GID'].isin(gids)].sort_values('FD pos')

    return ['GID'], 'Best Lineups', lambda df: df.reset_index('GID').groupby('Date').apply(
        select_best_lineup).reset_index(
        level=1, drop=True)


ROLLING = [5, 10, 15]


def rolling_average():
    def add_averages(df):
        grouped_df = df[['FDP']].groupby('GID')

        player_mean = grouped_df.transform(lambda s: s.expanding().mean().shift(periods=1))
        df = df.join(player_mean, rsuffix='_mean')

        for r in ROLLING:
            player_mean = grouped_df.transform(lambda s: s.rolling(r).mean().shift(periods=1))
            df = df.join(player_mean, rsuffix='_mean{}'.format(r))

        return df

    cols = ['FDP_mean{}'.format(r) for r in ROLLING] + ['FDP_mean']
    return cols, 'Rolling Average', add_averages


JOINED_STATS = [
    'Fgm',
    'Fga',
    'Fg3M',
    'Fg3A',
    'Ftm',
    'Fta',
    'Oreb',
    'Dreb',
    'Reb',
    'Ast',
    'Stl',
    'Blk',
    'To',
    'Pf',
    'Pts',
    'Win_Pct',
]

TEAM_STATS = [
    'Min',
    'Min_Big',
    'Min_PG',
    'Min_Wing',
    'Min_Avg_Big',
    'Min_Avg_PG',
    'Min_Avg_Wing',
    'Fgm',
    'Fga',
    'Fg3M',
    'Fg3A',
    'Ftm',
    'Fta',
    'Oreb',
    'Dreb',
    'Reb',
    'Ast',
    'Stl',
    'Blk',
    'To',
    'Pf',
    'Pts',
]


def join_opp():
    """
    Joins a Team-Date with the stat rows for the team in the Opp column

    :return:
    """

    def merge_opp(df: pd.DataFrame) -> pd.DataFrame:
        df = df.reset_index().set_index('Team')
        opp_df = df.reset_index().set_index(['Team', 'Date'])[JOINED_STATS]
        df = df.join(opp_df, rsuffix='_Opp', on=['Opp', 'Date'])
        return df.reset_index().set_index(['Team', 'Date'])

    return ['Pts_Opp'], 'Opponent Stats', merge_opp


TEAM_AVG_STATS = [
    'Pts_Opp',
    'Fg3M_Opp',
    'Reb_Opp',
    'Ast_Opp',
    'Blk_Opp',
    'Stl_Opp',
    'To_Opp',
    'Pf_Opp',
]

PLAYER_AVG_STATS = [
    'Min',
    'Pts',
    'Reb',
    'Fg3M',
    'Ftm',
    'Ast',
    'Stl',
    'Pf',
    'To',
    'Blk',
]


def join_with(df: pd.DataFrame, pkg: Package, cols, rsuffix=None) -> pd.DataFrame:
    df = df.join(pkg.build()[cols], rsuffix=rsuffix)
    return df


def rolling_avg(df: pd.DataFrame, cols, group) -> pd.DataFrame:
    df[cols] = df[cols].groupby(group).transform(lambda s: s.expanding().mean().shift(periods=1))
    return df


def normalize_cols(cols):
    def normalize(df: pd.DataFrame) -> pd.DataFrame:
        scaler = MinMaxScaler()
        df[cols] = scaler.fit_transform(df[cols])
        return df

    return [], 'Normalize', normalize


def join_role_min(player_df: pd.DataFrame, values) -> pd.DataFrame:
    join_df = player_df.reset_index().pivot_table(index=['GID', 'Date'], columns='Role', values=values)
    join_df.columns = join_df.columns.map('_'.join)
    player_df = player_df.join(join_df)
    return player_df


def group_teams(player_df: pd.DataFrame) -> pd.DataFrame:
    inj_df = player_df[player_df['Out'] == 0].reset_index().set_index(['Team', 'Date'])[
        ['Min_Avg_Big', 'Min_Avg_Wing', 'Min_Avg_PG']].groupby(['Team', 'Date']).sum()
    player_df = player_df.reset_index().set_index(['Team', 'Date'])[TEAM_STATS + ['Opp']]
    team_df = player_df[TEAM_STATS].groupby(['Team', 'Date']).sum()

    team_df = team_df.join(player_df[['Opp']].groupby(['Team', 'Date']).first())
    team_df = team_df.join(inj_df, rsuffix='_Avail')
    team_df = team_df.drop(columns=['Min_Avg_Big', 'Min_Avg_Wing', 'Min_Avg_PG'])
    team_df.reset_index().set_index('Team')
    opp_df = team_df.reset_index().set_index(['Team', 'Date'])
    team_df['Win'] = (team_df['Pts'] > (
        team_df.join(opp_df, on=['Opp', 'Date'], rsuffix='_Opp').reset_index().set_index(['Team', 'Date'])[
            'Pts_Opp'])).astype(int)
    team_df = team_df.join(team_df[['Win']].groupby('Team').transform(lambda s: s.expanding().mean().shift(periods=1)),
                           rsuffix='_Pct')
    return team_df


FANDUEL_ACTIVE = Package(base='historical/stats_team_avgs.csv', out='historical/stats_mean.csv',
                         index_col=['GID', 'Date']).with_rule(
    *sorted_rule(['GID', 'Date'])).with_rule(*only_played()).with_rule(*rolling_average())

FANDUEL_BASE = Package(base='historical/stats_team_avgs.csv', out='historical/fanduel_base.csv',
                       index_col=['GID', 'Date']).with_rule(*sorted_rule(['GID', 'Date']))

NBA_BOX = Package(base='historical/nba_games_box_full.csv', index_col=['GID', 'Date'])

NBA_BOX_AVG = Package(base=NBA_BOX, out='historical/nba_games_box_avg.csv', index_col=['GID', 'Date'],
                      usecols=PLAYER_AVG_STATS + ['Team'], rules=[
        sorted_rule(['GID', 'Date']),
        # normalize_cols(PLAYER_AVG_STATS),
        apply(rolling_avg, PLAYER_AVG_STATS, 'GID')
    ])

NBA_TEAMS_BOX = Package(base=NBA_BOX, out='historical/nba_teams_box.csv', index_col=['Team', 'Date'], rules=[
    apply(join_with, NBA_BOX_AVG, ['Min'], '_Avg'),
    apply(join_role_min, ['Min', 'Min_Avg']),
    apply(group_teams),
])

NBA_TEAMS_BOX_OPP = Package(base='historical/nba_teams_box.csv', out='historical/nba_teams_box_opp.csv',
                            index_col=['Team', 'Date'], rules=[join_opp()])

NBA_TEAMS_AVG = Package(base=NBA_TEAMS_BOX_OPP, out='historical/nba_teams_opp_avg.csv',
                        usecols=TEAM_AVG_STATS + ['Win_Pct', 'Win_Pct_Opp'], index_col=['Team', 'Date'],
                        rules=[
                            normalize_cols(TEAM_AVG_STATS),
                            apply(rolling_avg, TEAM_AVG_STATS, 'Team'),
                        ])

BEST_LINEUPS = Package(base=FANDUEL_ACTIVE, out='historical/best_lineups.csv', rules=[
    apply(pd.DataFrame.dropna),
    best_lineups()
])


class PlayerLookup:
    EXCEPTIONS = {
        'kelly oubre jr': 'kelly oubre',
        'otto porter jr': 'otto porter',
        'nene': 'nene hilario',
        'edy tavares': 'walter tavares',
        'lou amundson': 'louis amundson',
        'johnny obryant iii': 'johnny obryant',
        'lou williams': 'louis williams',
        'wesley matthews': 'wes matthews',
        'joe young': 'joseph young',
        'marcus morris sr': 'marcus morris',
        'jj barea': 'jose barea',
        'larry nance jr': 'larry nance',
        'james ennis iii': 'james ennis',
        'ish smith': 'ishmael smith',
        'mo williams': 'maurice williams',
    }

    @classmethod
    def _clean_name(cls, name: str) -> str:
        changed_name = name.replace('-', '')
        changed_name = changed_name.replace('.', '')
        changed_name = changed_name.replace("'", '')
        return changed_name.lower()

    def __init__(self, df: pd.DataFrame):
        self.gids = {}
        for index, row in df.groupby('GID').first()['First Last'].iteritems():
            self.gids[PlayerLookup._clean_name(row)] = index

    def lookup(self, name: str) -> Optional[str]:
        clean_name = PlayerLookup._clean_name(name)
        if clean_name in PlayerLookup.EXCEPTIONS:
            clean_name = PlayerLookup.EXCEPTIONS[clean_name]
        if clean_name not in self.gids:
            return None
        return self.gids[clean_name]
