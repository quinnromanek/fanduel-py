from pandas.core.frame import DataFrame
from player_model import all_gids, name_clean


class GIDFeature:
    KEY = "GID"

    def __init__(self):
        self._gids = all_gids()

    def row_value(self, row):
        name_id = name_clean(row["First Last"])
        if name_id in self._gids["Names"]:
            return self._gids["Names"][name_id]
        elif "Id" in row:
            return int(row["Id"].split("-")[-1])
        else:
            return None


def add_feature(dataframe: DataFrame, feature):
    if feature.KEY in dataframe:
        return False
    feature_row = []
    for _, row in dataframe.iterrows():
        feature_row.append(feature.row_value(row))
    dataframe[feature.KEY] = feature_row
    return True
